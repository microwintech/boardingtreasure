package com.microwintech.boardingtreasure.adapter.flightdynamic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;

import java.util.List;

/**
 * Created by Administrator on 2014/11/29.
 */
public class FlightInfoAdapter extends BaseAdapter {
    private Context context;
    private List<FlightInfo> flightList;
    private LayoutInflater layoutInflater;

    public FlightInfoAdapter(Context context, List<FlightInfo> flightList) {
        this.context = context;
        this.flightList = flightList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return flightList.size();
    }

    @Override
    public Object getItem(int i) {
        return flightList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.flight_query_item, null);
        }
        TextView flightCodeTv = (TextView) view.findViewById(R.id.flight_item_flight_code_tv);
        TextView carrierNameTv = (TextView) view.findViewById(R.id.flight_item_carrier_name_tv);
        TextView depCityTv = (TextView) view.findViewById(R.id.flight_item_dep_city_tv);
        TextView arrCityTv = (TextView) view.findViewById(R.id.flight_item_arr_city_tv);

        TextView planArrTimeTv = (TextView) view.findViewById(R.id.flight_item_plan_arr_time_tv);
        TextView planDepTimeTv = (TextView) view.findViewById(R.id.flight_item_plan_dep_time_tv);
        TextView actArrTimeTv = (TextView) view.findViewById(R.id.flight_item_act_arr_time_tv);
        TextView actDepTimeTv = (TextView) view.findViewById(R.id.flight_item_act_dep_time_tv);
        TextView flightStateTv = (TextView) view.findViewById(R.id.flight_item_flight_state_tv);
        FlightInfo info = flightList.get(i);
        flightCodeTv.setText(info.getFlightCode());
        carrierNameTv.setText(info.getCarrierName());
        depCityTv.setText(info.getDepCity());
        arrCityTv.setText(info.getArrCity());

        planArrTimeTv.setText(info.getPlanArrTime());
        planDepTimeTv.setText(info.getPlanDepTime());
        actArrTimeTv.setText(info.getActArrTime());
        actDepTimeTv.setText(info.getActDepTime());
        flightStateTv.setText(info.getFlightState());
        return view;
    }
}
