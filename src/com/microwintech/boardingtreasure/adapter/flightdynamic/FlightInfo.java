package com.microwintech.boardingtreasure.adapter.flightdynamic;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Administrator on 2014/11/29.
 * 航班动态信息
 */
public class FlightInfo {
        //{"ActArrTime":"09:15","ActDepTime":"07:34","ArrCity":"北京","ArrTerminal":"","CarrierName":"中国邮政航空公司","DepCity":"上海浦东","DepTerminal":"",
    // "FlightCode":"8Y9022","FlightState":"到达","PlanArrTime":"09:24","PlanDepTime":"04:25"}
    private String actArrTime;
    private String actDepTime;
    private String arrCity;
    private String arrTerminal;
    private String carrierName;
    private String depCity;
    private String depTerminal;
    private String flightCode;
    private String flightState;
    private String planArrTime;
    private String planDepTime;


    public String getActArrTime() {
        return actArrTime;
    }

    public void setActArrTime(String actArrTime) {
        this.actArrTime = actArrTime;
    }

    public String getActDepTime() {
        return actDepTime;
    }

    public void setActDepTime(String actDepTime) {
        this.actDepTime = actDepTime;
    }

    public String getArrCity() {
        return arrCity;
    }

    public void setArrCity(String arrCity) {
        this.arrCity = arrCity;
    }

    public String getArrTerminal() {
        return arrTerminal;
    }

    public void setArrTerminal(String arrTerminal) {
        this.arrTerminal = arrTerminal;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getDepCity() {
        return depCity;
    }

    public void setDepCity(String depCity) {
        this.depCity = depCity;
    }

    public String getDepTerminal() {
        return depTerminal;
    }

    public void setDepTerminal(String depTerminal) {
        this.depTerminal = depTerminal;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    public String getFlightState() {
        return flightState;
    }

    public void setFlightState(String flightState) {
        this.flightState = flightState;
    }

    public String getPlanArrTime() {
        return planArrTime;
    }

    public void setPlanArrTime(String planArrTime) {
        this.planArrTime = planArrTime;
    }

    public String getPlanDepTime() {
        return planDepTime;
    }

    public void setPlanDepTime(String planDepTime) {
        this.planDepTime = planDepTime;
    }


}
