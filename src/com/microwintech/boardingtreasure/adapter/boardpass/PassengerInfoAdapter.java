package com.microwintech.boardingtreasure.adapter.boardpass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.adapter.passengermanage.Passenger;
import com.microwintech.boardingtreasure.adapter.passengermanage.PassengerSelectInterface;

import java.util.List;

/**
 * Created by Administrator on 2014/12/3.
 */
public class PassengerInfoAdapter extends BaseAdapter {
    private Context context;
    private List<PassengerInfo> passengerList;
    private LayoutInflater layoutInflater;

    public PassengerInfoAdapter(Context context, List<PassengerInfo> passengerList) {
        this.context = context;
        this.passengerList = passengerList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return passengerList.size();
    }

    @Override
    public Object getItem(int i) {
        return passengerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.passenger_choose_passenger_info_item, null);
        }
        CheckBox whetherTv = (CheckBox) view.findViewById(R.id.passenger_item_info_cb);
        TextView nameTv = (TextView) view.findViewById(R.id.passenger_item_name_tv);
        TextView phoneNumberTv = (TextView) view.findViewById(R.id.passenger_item_phone_tv);
        TextView cardTv = (TextView) view.findViewById(R.id.passenger_item_card_tv);
        TextView cardNumberTv = (TextView) view.findViewById(R.id.passenger_item_idnumber_tv);
        PassengerInfo passenger = passengerList.get(i);
        whetherTv.setChecked(passenger.isWhetherSelect());
        nameTv.setText(passenger.getName());
        phoneNumberTv.setText(passenger.getPhoneNumber());
        cardTv.setText(passenger.getCard());
        cardNumberTv.setText(passenger.getCardNumber());
        view.setOnClickListener(new EditClickListener(passenger));
        return view;
    }

    class EditClickListener implements View.OnClickListener {
        PassengerInfo passenger;

        EditClickListener(PassengerInfo passenger) {
            this.passenger = passenger;
        }

        @Override
        public void onClick(View view) {
            if (passenger.isWhetherSelect()) {
                passenger.setWhetherSelect(false);
            } else {
                passenger.setWhetherSelect(true);
            }
        }
    }
}
