package com.microwintech.boardingtreasure.adapter.boardpass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.ui.boardpass.BoardPassInterface;

import java.util.List;

/**
 * Created by Administrator on 2014/12/13.
 */
public class BoardPassengerAdapter extends BaseAdapter {
    private Context context;
    private List<BoardPassenger> passengerList;
    private LayoutInflater layoutInflater;
    BoardPassInterface boardPassInterface;

    public BoardPassengerAdapter(Context context, List<BoardPassenger> passengerList, BoardPassInterface boardPassInterface) {
        this.context = context;
        this.passengerList = passengerList;
        this.boardPassInterface = boardPassInterface;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return passengerList.size();
    }

    @Override
    public Object getItem(int i) {
        return passengerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.boarding_pass_passenger_item, null);
        }
        TextView nameTv = (TextView) view.findViewById(R.id.passenger_item_name_tv);
        TextView codeTv = (TextView) view.findViewById(R.id.passenger_item_phone_tv);
        TextView dateTv = (TextView) view.findViewById(R.id.passenger_item_card_tv);
        LinearLayout deleteLl = (LinearLayout) view.findViewById(R.id.board_pass_delete_ll);

        BoardPassenger passenger = passengerList.get(i);
        nameTv.setText(passenger.getName());
        codeTv.setText(passenger.getPhone());
        dateTv.setText(passenger.getCardNumber());
        deleteLl.setOnClickListener(new DeleteClickListener(passenger));

        return view;
    }

    class DeleteClickListener implements View.OnClickListener {

        BoardPassenger passenger;

        DeleteClickListener(BoardPassenger passenger) {
            this.passenger = passenger;
        }

        @Override
        public void onClick(View view) {
            boardPassInterface.gainBoardResult(passenger);
        }
    }
}


