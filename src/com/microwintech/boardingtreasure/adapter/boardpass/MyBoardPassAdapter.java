package com.microwintech.boardingtreasure.adapter.boardpass;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.ui.boardpass.MyBoardingPassActivity;
import com.microwintech.boardingtreasure.ui.onlinepayment.OnlinePaymentActivity;

import java.util.List;

/**
 * Created by Administrator on 2014/12/4.
 */
public class MyBoardPassAdapter extends BaseAdapter {
    private Context context;
    private List<MyBoardPass> passengerList;
    private LayoutInflater layoutInflater;

    public MyBoardPassAdapter(Context context, List<MyBoardPass> passengerList) {
        this.context = context;
        this.passengerList = passengerList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return passengerList.size();
    }

    @Override
    public Object getItem(int i) {
        return passengerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = layoutInflater.inflate(R.layout.my_board_pass_item, null);
            holder.nameTv = (TextView) view.findViewById(R.id.my_board_pass_name_tv);
            holder.codeTv = (TextView) view.findViewById(R.id.my_board_pass_flight_code_tv);
            holder.dateTv = (TextView) view.findViewById(R.id.my_board_pass_flight_date_tv);
            holder.airportTv = (TextView) view.findViewById(R.id.my_board_pass_flight_airport_tv);
            holder.arrTv = (TextView) view.findViewById(R.id.my_board_pass_flight_arr_port_tv);
            holder.statusTv = (TextView) view.findViewById(R.id.my_board_pass_flight_status_tv);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        MyBoardPass passenger = passengerList.get(i);
        holder.nameTv.setText(passenger.getUserName());
        holder.codeTv.setText(passenger.getFlightCode());
        holder.dateTv.setText(passenger.getFlightDate());
        holder.airportTv.setText(passenger.getArrAirport());
        holder.arrTv.setText(passenger.getDepAirport());
        holder.statusTv.setText(passenger.getStatus());
        if (passenger.getStatus().trim().contains("待付")) {
            holder.statusTv.setOnClickListener(new EditClickListener(passenger));
            holder.statusTv.setTextColor(context.getResources().getColor(R.color.white));
            holder.statusTv.setBackgroundColor(context.getResources().getColor(R.color.gold));
        } else {
            holder.statusTv.setTextColor(context.getResources().getColor(R.color.black));
            holder.statusTv.setBackgroundColor(context.getResources().getColor(R.color.common_content_bg_color));
        }
        return view;
    }

    class EditClickListener implements View.OnClickListener {
        MyBoardPass passenger;

        EditClickListener(MyBoardPass passenger) {
            this.passenger = passenger;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, OnlinePaymentActivity.class);
            intent.putExtra("orderId", passenger.getOrderId());
            context.startActivity(intent);

        }
    }

    class ViewHolder {
        TextView nameTv;
        TextView codeTv;
        TextView dateTv;
        TextView airportTv;
        TextView arrTv;
        TextView statusTv;

    }

}
