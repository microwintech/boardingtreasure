package com.microwintech.boardingtreasure.adapter.boardpass;

/**
 * Created by Administrator on 2014/12/13.
 */
public class BoardPassenger {
    private String name;
    private String phone;
    private String cardNumber;
    private String preferArea;
    private String preferSeat;
    private String status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getPreferArea() {
        return preferArea;
    }

    public void setPreferArea(String preferArea) {
        this.preferArea = preferArea;
    }

    public String getPreferSeat() {
        return preferSeat;
    }

    public void setPreferSeat(String preferSeat) {
        this.preferSeat = preferSeat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
