package com.microwintech.boardingtreasure.adapter.boardpass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;

import java.util.List;

/**
 * Created by Administrator on 2014/12/4.
 */
public class BoardResultAdapter extends BaseAdapter {
    private Context context;
    private List<BoardResult> passengerList;
    private LayoutInflater layoutInflater;

    public BoardResultAdapter(Context context, List<BoardResult> passengerList) {
        this.context = context;
        this.passengerList = passengerList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return passengerList.size();
    }

    @Override
    public Object getItem(int i) {
        return passengerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.boarding_pass_result_item, null);
        }
        TextView nameTv = (TextView) view.findViewById(R.id.board_pass_result_name_tv);
        TextView codeTv = (TextView) view.findViewById(R.id.board_pass_result_phone_tv);
        TextView dateTv = (TextView) view.findViewById(R.id.board_pass_result_card_tv);
        BoardResult passenger = passengerList.get(i);
        nameTv.setText(passenger.getName());
        codeTv.setText(passenger.getPhone());
        dateTv.setText(passenger.getCardNumber());

        view.setOnClickListener(new EditClickListener(passenger));
        return view;
    }

    class EditClickListener implements View.OnClickListener {
        BoardResult passenger;

        EditClickListener(BoardResult passenger) {
            this.passenger = passenger;
        }

        @Override
        public void onClick(View view) {

        }
    }
}

