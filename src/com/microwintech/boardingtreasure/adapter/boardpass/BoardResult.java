package com.microwintech.boardingtreasure.adapter.boardpass;

/**
 * Created by Administrator on 2014/12/4.
 */
public class BoardResult {
    private String name;
    private String phone;
    private String cardNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
