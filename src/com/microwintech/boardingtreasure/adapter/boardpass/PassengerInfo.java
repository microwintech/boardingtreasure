package com.microwintech.boardingtreasure.adapter.boardpass;

/**
 * Created by Administrator on 2014/12/3.
 * 乘机人信息
 */
public class PassengerInfo {
    private boolean whetherSelect;
    private String name;
    private String phoneNumber;
    private String card;
    private String cardNumber;

    public boolean isWhetherSelect() {
        return whetherSelect;
    }

    public void setWhetherSelect(boolean whetherSelect) {
        this.whetherSelect = whetherSelect;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
