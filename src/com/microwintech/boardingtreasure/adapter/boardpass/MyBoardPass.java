package com.microwintech.boardingtreasure.adapter.boardpass;

/**
 * Created by Administrator on 2014/12/3.
 * 我的登机牌
 */
public class MyBoardPass {
    //    "UserName": "\u5353\u73b2\u73b2",
//            "FlightCode": "CA1936",
//            "FlightDate": "2014-11-28",
//            "DepAirport": "\u5e7f\u5dde\u767d\u4e91",
//            "ArrAirport": "\u4e0a\u6d77\u6d66\u4e1c",
//            "Status": "\u529e\u7406\u4e2d"
    private String userName;
    private String orderId;//订单id
    private String flightCode;
    private String flightDate;
    private String depAirport;
    private String arrAirport;
    private String status;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    public String getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(String flightDate) {
        this.flightDate = flightDate;
    }

    public String getDepAirport() {
        return depAirport;
    }

    public void setDepAirport(String depAirport) {
        this.depAirport = depAirport;
    }

    public String getArrAirport() {
        return arrAirport;
    }

    public void setArrAirport(String arrAirport) {
        this.arrAirport = arrAirport;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
