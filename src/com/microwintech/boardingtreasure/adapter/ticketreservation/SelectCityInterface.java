package com.microwintech.boardingtreasure.adapter.ticketreservation;

import com.microwintech.boardingtreasure.db.entity.CityAirport;

/**
 * Created by Administrator on 2014/11/29.
 */
public interface SelectCityInterface {
    public void selectedCity(CityAirport cityAirport);
}
