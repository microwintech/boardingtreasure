package com.microwintech.boardingtreasure.adapter.ticketreservation;


/**
 * Created by Administrator on 2014/11/6.
 */
public class City {
    private String cityName;
    private String pinYinName;

    public City(String cityName) {
        this.cityName = cityName;
    }

    public City(String cityName, String pinYinName) {
        this.cityName = cityName;
        this.pinYinName = pinYinName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPinYinName() {
        return pinYinName;
    }

    public void setPinYinName(String pinYinName) {
        this.pinYinName = pinYinName;
    }
}
