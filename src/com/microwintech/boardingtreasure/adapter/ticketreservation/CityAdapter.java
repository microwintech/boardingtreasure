package com.microwintech.boardingtreasure.adapter.ticketreservation;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.db.entity.CityAirport;

import java.util.List;

/**
 * Created by Administrator on 2014/11/6.
 */
public class CityAdapter extends BaseAdapter {
    private Context context;
    private List<CityAirport> list;
    private ViewHolder viewHolder;
    private SelectCityInterface cityInterface;

    public CityAdapter(Context context, List<CityAirport> list, SelectCityInterface cityInterface) {
        this.context = context;
        this.list = list;
        this.cityInterface = cityInterface;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position) {
        if (list.get(position).getCityName().length() == 1)// 如果是字母索引
            return false;// 表示不能点击
        return super.isEnabled(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CityAirport city = list.get(position);
        String item = city.getCityName();
        viewHolder = new ViewHolder();
        if (item.length() == 1) {
            convertView = LayoutInflater.from(context).inflate(R.layout.hot_city_index, null);
            viewHolder.indexTv = (TextView) convertView.findViewById(R.id.indexTv);
        } else {
            convertView = LayoutInflater.from(context).inflate(R.layout.hot_city_item, null);
            viewHolder.itemTv = (TextView) convertView.findViewById(R.id.itemTv);
        }
        if (item.length() == 1) {
            String cityName = city.getCityName();
            if ("#".equals(cityName)) {
                viewHolder.indexTv.setText("热门城市");
            } else {
                viewHolder.indexTv.setText(cityName);
            }
        } else {
            String cityName = city.getCityName();
            if (cityName.contains("#")) {
                viewHolder.itemTv.setText(cityName.substring(1, cityName.length()));
            } else {
                viewHolder.itemTv.setText(cityName);
            }
        }
        convertView.setOnClickListener(new SelectItemClick(city));
        return convertView;
    }

    class SelectItemClick implements View.OnClickListener {
        CityAirport cityAirport;

        SelectItemClick(CityAirport cityAirport) {
            this.cityAirport = cityAirport;
        }

        @Override
        public void onClick(View view) {
            cityInterface.selectedCity(cityAirport);
        }
    }


    private class ViewHolder {
        private TextView indexTv;
        private TextView itemTv;
    }
}
