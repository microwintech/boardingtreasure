package com.microwintech.boardingtreasure.adapter.passengermanage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.ui.BaseFragment;
import com.microwintech.boardingtreasure.ui.flightdynamic.FlightDynamicFragment;

import java.util.List;

/**
 * Created by Administrator on 2014/11/26.
 */
public class PassengerAdapter extends BaseAdapter {
    private Context context;
    private List<Passenger> passengerList;
    private LayoutInflater layoutInflater;
    private PassengerSelectInterface passengerInterface;

    public PassengerAdapter(Context context, List<Passenger> passengerList, PassengerSelectInterface passengerInterface) {
        this.context = context;
        this.passengerList = passengerList;
        this.passengerInterface = passengerInterface;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return passengerList.size();
    }

    @Override
    public Object getItem(int i) {
        return passengerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.passenger_manage_item, null);
        }
        TextView nameTv = (TextView) view.findViewById(R.id.passenger_manage_item_name_tv);
        TextView phoneTv = (TextView) view.findViewById(R.id.passenger_manage_item_phone_tv);
        TextView cardTv = (TextView) view.findViewById(R.id.passenger_manage_item_card_tv);
        TextView numberTv = (TextView) view.findViewById(R.id.passenger_manage_item_number_tv);
        Passenger passenger = passengerList.get(i);
        nameTv.setText(passenger.getName());
        phoneTv.setText(passenger.getPhoneNumber());
        cardTv.setText(passenger.getCardName());
        numberTv.setText(passenger.getCardNumber());
        view.setOnClickListener(new EditClickListener(passenger));
        return view;
    }

    class EditClickListener implements View.OnClickListener {
        Passenger passenger;

        EditClickListener(Passenger passenger) {
            this.passenger = passenger;
        }

        @Override
        public void onClick(View view) {
            passengerInterface.selectPassenger(passenger);

        }
    }
}
