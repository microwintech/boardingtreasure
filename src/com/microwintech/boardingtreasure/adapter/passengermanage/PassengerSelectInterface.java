package com.microwintech.boardingtreasure.adapter.passengermanage;

/**
 * Created by Administrator on 2014/11/30.
 * 乘机人选择接口
 */
public interface PassengerSelectInterface {
    public void selectPassenger(Passenger passenger);
}
