package com.microwintech.boardingtreasure.adapter.passengermanage;

import com.microwintech.boardingtreasure.ui.register.SetPasswordActivity;

/**
 * Created by Administrator on 2014/11/26.
 */
public class Passenger {

    //                "UserGuid":"bd010d0a-c66b-4609-8c30-e4e58f19a3a2",
//                        "OftenContactID":"bd010d0a-c66b-4609-8c30-e4e58f19a3a2",
//                        "Name":"王振柱",
//                        "Mobile":"13818342667",
//                        "CardType":"身份证",
//                        "CardNo":"372426197502212015",
    private String uuid;
    private String oftenContactId;
    private String name;//姓名
    private String phoneNumber;//手机号码
    private String cardName;//卡类型
    private String cardNumber;//卡号
    private String preferArea = "";
    private String preferSeat = "";
    private boolean whetherSelect = false;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getOftenContactId() {
        return oftenContactId;
    }

    public void setOftenContactId(String oftenContactId) {
        this.oftenContactId = oftenContactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getPreferArea() {
        return preferArea;
    }

    public void setPreferArea(String preferArea) {
        this.preferArea = preferArea;
    }

    public String getPreferSeat() {
        return preferSeat;
    }

    public void setPreferSeat(String preferSeat) {
        this.preferSeat = preferSeat;
    }

    public boolean isWhetherSelect() {
        return whetherSelect;
    }

    public void setWhetherSelect(boolean whetherSelect) {
        this.whetherSelect = whetherSelect;
    }
}
