package com.microwintech.boardingtreasure.adapter.myjourney;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.adapter.boardpass.BoardResult;
import com.microwintech.boardingtreasure.ui.boardpass.BoardingPassActivity;

import java.util.List;

/**
 * Created by Administrator on 2014/12/4.
 */
public class JourneyAdapter extends BaseAdapter {
    private Context context;
    private List<Journey> passengerList;
    private LayoutInflater layoutInflater;

    public JourneyAdapter(Context context, List<Journey> passengerList) {
        this.context = context;
        this.passengerList = passengerList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return passengerList.size();
    }

    @Override
    public Object getItem(int i) {
        return passengerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.my_journey_item, null);
        }
        //姓名
        TextView nameTv = (TextView) view.findViewById(R.id.my_journey_name_tv);
        //航班号
        TextView codeTv = (TextView) view.findViewById(R.id.my_journey_fight_code_tv);
        //航班日期
        TextView dateTv = (TextView) view.findViewById(R.id.my_journey_fight_date_tv);
        //出发城市
        TextView depAirTv = (TextView) view.findViewById(R.id.my_journey_dep_air_tv);
        //办理登机牌
        TextView toolTv = (TextView) view.findViewById(R.id.my_journey_tool_tv);

        TextView arrAirTv = (TextView) view.findViewById(R.id.my_journey_arr_air_tv);

        Journey passenger = passengerList.get(i);
        nameTv.setText(passenger.getName());
        codeTv.setText(passenger.getFlightCode());
        dateTv.setText(passenger.getDepDate());
        depAirTv.setText(passenger.getDepAirportName());
        if (passenger.isWhetherNeed()) {
            toolTv.setVisibility(View.VISIBLE);
            toolTv.setOnClickListener(new EditClickListener(passenger));
        } else {
          //  toolTv.setVisibility(View.INVISIBLE);
        }

        arrAirTv.setText(passenger.getArrAirportName());

        return view;
    }

    class EditClickListener implements View.OnClickListener {
        Journey passenger;

        EditClickListener(Journey passenger) {
            this.passenger = passenger;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, BoardingPassActivity.class);
            intent.putExtra("flight_data", "data");
            intent.putExtra("flight_code", passenger.getFlightCode());
            intent.putExtra("flight_date", passenger.getDepDate());
            context.startActivity(intent);
        }
    }


}
