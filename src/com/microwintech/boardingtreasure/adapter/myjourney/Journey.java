package com.microwintech.boardingtreasure.adapter.myjourney;

/**
 * Created by Administrator on 2014/12/4.
 */
public class Journey {
    private String name;//乘机人姓名
    private String ticketNo;
    private String depDate;
    private String arrDate;
    private String flightCode;//航班号T
    private String depAirportCode;
    private String arrAirportCode;
    private String depAirportName;//出发机场
    private String arrAirportName;//到达机场
    private boolean whetherNeed;//是否需要办理登机
//    "TicketNo": "781-2486065432",
//            "PassengerName": "尚正通",
//            "DepDate": "2014-12-16",
//            "ArrDate": null,
//            "FlightCode": "MU5215",
//            "DepAirportCode": "HGH",
//            "DepAirportName": "杭州市",
//            "ArrAirportCode": "CAN",
//            "ArrAirportName": "广州市"


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getDepDate() {
        return depDate;
    }

    public void setDepDate(String depDate) {
        this.depDate = depDate;
    }

    public String getArrDate() {
        return arrDate;
    }

    public void setArrDate(String arrDate) {
        this.arrDate = arrDate;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    public String getDepAirportCode() {
        return depAirportCode;
    }

    public void setDepAirportCode(String depAirportCode) {
        this.depAirportCode = depAirportCode;
    }

    public String getArrAirportCode() {
        return arrAirportCode;
    }

    public void setArrAirportCode(String arrAirportCode) {
        this.arrAirportCode = arrAirportCode;
    }

    public String getDepAirportName() {
        return depAirportName;
    }

    public void setDepAirportName(String depAirportName) {
        this.depAirportName = depAirportName;
    }

    public String getArrAirportName() {
        return arrAirportName;
    }

    public void setArrAirportName(String arrAirportName) {
        this.arrAirportName = arrAirportName;
    }

    public boolean isWhetherNeed() {
        return whetherNeed;
    }

    public void setWhetherNeed(boolean whetherNeed) {
        this.whetherNeed = whetherNeed;
    }
}
