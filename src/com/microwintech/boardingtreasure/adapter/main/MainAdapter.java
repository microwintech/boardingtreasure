package com.microwintech.boardingtreasure.adapter.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.ui.boardpass.BoardingPassActivity;
import com.microwintech.boardingtreasure.ui.flightdynamic.FlightDynamicActivity;
import com.microwintech.boardingtreasure.ui.ticketreservation.TicketReserveActivity;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

import java.util.List;

/**
 * Created by Administrator on 2014/10/29.
 */
public class MainAdapter extends BaseAdapter {
    private Context context;
    private List<Module> moduleList;
    private LayoutInflater layoutInflater;

    public MainAdapter(Context context, List<Module> moduleList) {
        this.context = context;
        this.moduleList = moduleList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return moduleList.size();
    }

    @Override
    public Object getItem(int i) {
        return moduleList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.main_module_item, null);
        }
        EffectsButton itemEb = (EffectsButton) view.findViewById(R.id.module_item_eb);
        Module module = moduleList.get(i);
        Drawable topDrawable = context.getResources().getDrawable(module.getResId());
        itemEb.setCompoundDrawablesWithIntrinsicBounds(null, topDrawable, null, null);
//        itemEb.setCompoundDrawablePadding(-20);
        itemEb.setText(module.getTitle());
        itemEb.setOnClickListener(new MainOnClickListener(module.getResId()));
        return view;
    }

    class MainOnClickListener implements View.OnClickListener {
        int resId;

        MainOnClickListener(int resId) {
            this.resId = resId;
        }

        @Override
        public void onClick(View view) {
            switch (resId) {
                case R.drawable.banlidengjipai: {
                    //办理登机牌
                    Intent mainIntent = new Intent(context, BoardingPassActivity.class);
                    context.startActivity(mainIntent);
                    break;
                }
                case R.drawable.jipiaoyuding: {
                    //机票预订
                    Intent mainIntent = new Intent(context, TicketReserveActivity.class);
                    context.startActivity(mainIntent);
                    break;
                }
                case R.drawable.wodemudan: {
                    //我的牡丹
//                    Intent mainIntent = new Intent(context, MyPeonyActivity.class);
//                    context.startActivity(mainIntent);
                    break;
                }
                case R.drawable.hangbandongtai: {
                    //我的牡丹
                    Intent flightIntent = new Intent(context, FlightDynamicActivity.class);
                    context.startActivity(flightIntent);

                    break;
                }
            }
        }
    }

}
