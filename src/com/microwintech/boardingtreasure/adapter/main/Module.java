package com.microwintech.boardingtreasure.adapter.main;

/**
 * Created by Administrator on 2014/10/29.
 */
public class Module {
    private int resId;
    private String title;

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
