package com.microwintech.boardingtreasure.adapter.register;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;

import java.util.List;

/**
 * Created by Administrator on 2014/11/25.
 */
public class SpaceAdapter extends BaseAdapter {
    private List<String> stringList;
    private Context context;
    private LayoutInflater layoutInflater;

    public SpaceAdapter(Context context, List<String> stringList) {
        this.context = context;
        this.stringList = stringList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int i) {
        return stringList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.perfect_info_like_item, null);
        }
        TextView likeTv = (TextView) view.findViewById(R.id.perfect_info_like_tv);
        likeTv.setText(stringList.get(i));
        return view;
    }
}
