package com.microwintech.boardingtreasure.adapter.myticketorders;

import java.io.Serializable;

/**
 * Created by Administrator on 2014/11/3.
 */
public class Order implements Serializable {
    private String startEndPoint;//起终点
    private String departureTime;//起飞时间
    private String airport;//起降机场
    private String flight;//航班
    private String ticketPrice;//票价

    public String getStartEndPoint() {
        return startEndPoint;
    }

    public void setStartEndPoint(String startEndPoint) {
        this.startEndPoint = startEndPoint;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(String ticketPrice) {
        this.ticketPrice = ticketPrice;
    }
}
