package com.microwintech.boardingtreasure.adapter.myticketorders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

import java.util.List;

/**
 * Created by Administrator on 2014/11/3.
 */
public class OrderAdapter extends BaseAdapter {
    private Context context;
    private List<Order> orderList;
    private LayoutInflater layoutInflater;

    public OrderAdapter(Context context, List<Order> orderList) {
        this.context = context;
        this.orderList = orderList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return orderList.size();
    }

    @Override
    public Object getItem(int i) {
        return orderList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.my_ticket_orders_item, null);
        }
        EffectsButton noPaymentEb = (EffectsButton) view.findViewById(R.id.my_no_payment_eb);
        TextView startEndTv = (TextView) view.findViewById(R.id.myticket_start_end_point_tv);
        TextView departureTimeTv = (TextView) view.findViewById(R.id.myticket_departure_time_tv);
        TextView airportTv = (TextView) view.findViewById(R.id.myticket_airport_tv);
        TextView flightTv = (TextView) view.findViewById(R.id.myticket_flight_tv);
        TextView ticketPriceTv = (TextView) view.findViewById(R.id.myticket_ticket_price_tv);
        Order module = orderList.get(i);
        startEndTv.setText(module.getStartEndPoint());
        departureTimeTv.setText(module.getDepartureTime());
        airportTv.setText(module.getAirport());
        flightTv.setText(module.getFlight());
        ticketPriceTv.setText(module.getTicketPrice());
        return view;
    }
}
