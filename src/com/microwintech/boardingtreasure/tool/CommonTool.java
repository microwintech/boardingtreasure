package com.microwintech.boardingtreasure.tool;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;

/**
 * Created by Administrator on 2014/11/26.
 */
public class CommonTool {
    //   1ae92539e558a89eb0128c0179f8867a                35E91564CC53146C0BA59471CDE66D452A5E044C
    public static final String PROJECT_NAME = "boardingtreasure";
    public static final String SHJI_PATH = android.os.Environment.getDataDirectory().getAbsolutePath() + "/" + PROJECT_NAME;
    public static final String DATABASE_PATH = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + PROJECT_NAME;//路径
    public static final String DATABASE_FILENAME = "boardingtreasure.db";//数据库名
    public static final String DATA_FILE_PATH = existSDCard() ? DATABASE_PATH : SHJI_PATH;
    //6cb3f458a482fcd009414370808ea219
    /**
     * ******存放图片的文件夹*****************
     */
    public static final String DATA_IMAGE_FILE = DATA_FILE_PATH + "/Image";
    public static final String DATA_VIDEO_FILE = DATA_FILE_PATH + "/Video";
    public static final String DATA_TEMP_FILE = DATA_FILE_PATH + "/Temp";
    public static final String DATA_CACHE_FILE = DATA_FILE_PATH + "/Cache";
    public static final String DATA_SAFETY_FILE = DATA_FILE_PATH + "/AB98-457C-7845-5478";

    /**
     * 获取串号
     */
    public static String getDeviceNumber(Context context) {
        TelephonyManager telManage = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            return telManage.getDeviceId();
        } catch (Exception e) {
            return null;
        }
    }

    public static String getLine1Number(Context context) {
        TelephonyManager telManage = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            return telManage.getLine1Number();
        } catch (Exception e) {
            return null;
        }
    }

    //判断是否存在网络
    public static boolean isNotNetworkAvailable(Context context) {
        return !isNetworkAvailable(context);
    }

    //判断是否存在网络
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if ((info[i].getState() == NetworkInfo.State.CONNECTED) || (info[i].getState() == NetworkInfo.State.CONNECTING)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //判断SD卡是否存在
    public static boolean existSDCard() {
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isGpsOpen(Context context) {
        LocationManager alm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (alm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        }
        return false;
    }

}
