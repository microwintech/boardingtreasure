package com.microwintech.boardingtreasure.db.entity;

/**
 * Created by Administrator on 2014/11/23
 * 城市机场
 */
public class CityAirport {
    private String threeLetterCode;//三字码
    private String airportName;
    private String cityName;
    private String whetherHot;
    private String pinYinName;

    public CityAirport() {
    }

    public CityAirport(String cityName) {
        this.cityName = cityName;
    }

    public String getPinYinName() {
        return pinYinName;
    }

    public void setPinYinName(String pinYinName) {
        this.pinYinName = pinYinName;
    }
    //Whether the hot


    public String getThreeLetterCode() {
        return threeLetterCode;
    }

    public void setThreeLetterCode(String threeLetterCode) {
        this.threeLetterCode = threeLetterCode;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getWhetherHot() {
        return whetherHot;
    }

    public void setWhetherHot(String whetherHot) {
        this.whetherHot = whetherHot;
    }
}
