package com.microwintech.boardingtreasure.db.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.microwintech.boardingtreasure.db.entity.CityAirport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014/11/23.
 */
public class CityAirportDao {
    private Context context;

    public CityAirportDao(Context context) {
        this.context = context;
    }

    public List<CityAirport> getApplyList() {
        CityOpenHelper helper = null;
        SQLiteDatabase sqliteDatabase = null;
        Cursor cursor = null;
        List<CityAirport> daiPiBeans = new ArrayList<CityAirport>();
        try {
            helper = CityOpenHelper.getInstance(context);
            sqliteDatabase = helper.getReadableDatabase();
            // 调用SQLiteDatabase对象的query方法进行查询，返回一个Cursor对象：由数据库查询返回的结果集对象
            // 第一个参数String：表名     第二个参数String[]:要查询的列名   第三个参数String：查询条件
            // 第四个参数String[]：查询条件的参数   第五个参数String:对查询的结果进行分组  getReadableDatabase
            // 第六个参数String：对分组的结果进行限制     第七个参数String：对查询的结果进行排序
            String[] clumnArray = new String[]{"three_letter_code", "airport_name", "city_name", "whether_hot"};
            cursor = sqliteDatabase.query("city_airport", clumnArray, null, null, null, null, null);
            while (cursor.moveToNext()) {
                CityAirport apply = getCityAirport(cursor);
                daiPiBeans.add(apply);
            }
        } catch (Exception e) {
            System.out.println("异常信息");
            System.out.println(e.getMessage());
        } finally {
            helper.closeAll(cursor, sqliteDatabase, helper);
        }
        return daiPiBeans;
    }

    public List<CityAirport> searchApplyListByCondition(String city) {
        CityOpenHelper helper = null;
        SQLiteDatabase sqliteDatabase = null;
        Cursor cursor = null;
        List<CityAirport> daiPiBeans = new ArrayList<CityAirport>();
        try {
            helper = CityOpenHelper.getInstance(context);
            sqliteDatabase = helper.getReadableDatabase();
            // 调用SQLiteDatabase对象的query方法进行查询，返回一个Cursor对象：由数据库查询返回的结果集对象
            // 第一个参数String：表名     第二个参数String[]:要查询的列名   第三个参数String：查询条件
            // 第四个参数String[]：查询条件的参数   第五个参数String:对查询的结果进行分组  getReadableDatabase
            // 第六个参数String：对分组的结果进行限制     第七个参数String：对查询的结果进行排序
            String[] clumnArray = new String[]{"three_letter_code", "airport_name", "city_name", "whether_hot"};
            city = "%" + city + "%";
            String[] selectons = new String[]{city};
            cursor = sqliteDatabase.query("city_airport", clumnArray, "city_name like ? ", selectons, null, null, null);
            while (cursor.moveToNext()) {
                CityAirport apply = getCityAirport(cursor);
                daiPiBeans.add(apply);
            }
        } catch (Exception e) {
            System.out.println("异常信息");
            System.out.println(e.getMessage());
        } finally {
            helper.closeAll(cursor, sqliteDatabase, helper);
        }
        return daiPiBeans;
    }


    private CityAirport getCityAirport(Cursor cursor) {
        CityAirport info = new CityAirport();
        info.setThreeLetterCode(cursor.getString(cursor.getColumnIndex("three_letter_code")));
        info.setAirportName(cursor.getString(cursor.getColumnIndex("airport_name")));
        info.setCityName(cursor.getString(cursor.getColumnIndex("city_name")));
        info.setWhetherHot(cursor.getString(cursor.getColumnIndex("whether_hot")));
        return info;
    }
}
