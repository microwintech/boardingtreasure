package com.microwintech.boardingtreasure.db.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2014/11/16.
 */
public class UserInfo implements Serializable {
    private String userGuid = "";//用户uuid
    private String phoneNumber = "";//手机号
    private String password = "";//密码
    private String oftenContactID;//
    private String cardNo = "";//身份证号
    private String preferArea = "";//喜好
    private String preferSeat = "";//座位
    private boolean whetherLogin = false;
    private String name;

    public boolean isWhetherLogin() {
        return whetherLogin;
    }

    public void setWhetherLogin(boolean whetherLogin) {
        this.whetherLogin = whetherLogin;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public void setUserGuid(String userGuid) {
        this.userGuid = userGuid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOftenContactID() {
        return oftenContactID;
    }

    public void setOftenContactID(String oftenContactID) {
        this.oftenContactID = oftenContactID;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getPreferArea() {
        return preferArea;
    }

    public void setPreferArea(String preferArea) {
        this.preferArea = preferArea;
    }

    public String getPreferSeat() {
        return preferSeat;
    }

    public void setPreferSeat(String preferSeat) {
        this.preferSeat = preferSeat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
