package com.microwintech.boardingtreasure.db.cache;

import android.content.Context;
import android.content.SharedPreferences;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.HashMap;

/**
 * Created by Administrator on 2014/11/16.
 */
public class UserCache {
    private static UserCache cache;
    private HashMap<String, UserReference> userReferences;  //用于cache内容的存储
    private ReferenceQueue<UserInfo> userQueue;//垃圾reference的队列

    //集成softreference 使得每一个实例都具有可识别的标示
    //并且该标示与其在hashmap内的key一致
    private class UserReference extends SoftReference<UserInfo> {
        private String _key = "";

        public UserReference(UserInfo user, ReferenceQueue<UserInfo> userQueue) {
            super(user, userQueue);
            _key = Constant.CACHE_USER;
        }
    }

    //构建缓冲器实例
    private UserCache() {
        userReferences = new HashMap<String, UserReference>();
        userQueue = new ReferenceQueue<UserInfo>();
    }

    //取得缓冲器实例
    public static UserCache getInstance() {
        if (cache == null) {
            cache = new UserCache();
        }
        return cache;
    }

    //以软引用的方式对一个Employee对象的实例进行引用保存该引用
    private void cacheUser(UserInfo user) {
        clearCache();//清除垃圾引用
        UserReference ref = new UserReference(user, userQueue);
        userReferences.put(Constant.CACHE_USER, ref);
    }

    // private String userGuid;//用户uuid
//   private String phoneNumber;//手机号
//    private String password;//密码
//    private String oftenContactID;//
//    private String cardNo;//身份证号
//    private String preferArea;//喜好
//    private String preferSeat;//座位
//    private boolean whetherLogin = false;
//

    public void addUser(Context context, UserInfo userInfo) {
        SharedPreferences sharePre = context.getSharedPreferences(Constant.CACHE_USER, 0);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putString("phoneNumber", userInfo.getPhoneNumber());
        editor.putString("password", userInfo.getPassword());
        editor.putString("name", userInfo.getName());
        editor.putString("userGuid", userInfo.getUserGuid());
        editor.putBoolean("whetherLogin", userInfo.isWhetherLogin());
        editor.putString("PreferSeat", userInfo.getPreferSeat());
        editor.putString("PreferArea", userInfo.getPreferArea());
        editor.putString("OftenContactID", userInfo.getOftenContactID());
        editor.putString("CardNo", userInfo.getCardNo());
        cacheUser(userInfo);
        //"PreferArea":"前排","PreferSeat":"靠窗"
        editor.commit();
    }

    //进入家园设置
    public void modifyUser(Context context, UserInfo userInfo) {
        SharedPreferences sharePre = context.getSharedPreferences(Constant.CACHE_USER, 0);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putBoolean("oftenContactID", userInfo.isWhetherLogin());
        editor.putString("cardNo", userInfo.getPhoneNumber());
        editor.putString("preferArea", userInfo.getPassword());
        editor.putString("preferSeat", userInfo.getUserGuid());
        editor.commit();
    }

    //进入家园设置
    public void modifyLogin(Context context, boolean loginType) {
        UserInfo userInfo = getUser(context);
        SharedPreferences sharePre = context.getSharedPreferences(Constant.CACHE_USER, 0);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putString("phoneNumber", userInfo.getPhoneNumber());
        editor.putString("password", userInfo.getPassword());
        editor.putString("userGuid", userInfo.getUserGuid());
        editor.putBoolean("whetherLogin", loginType);
        editor.commit();
    }

    //根据所指定的标示，重新获取相应user对象的实例
    public UserInfo getUser(Context context) {
        UserInfo user = null;
        //缓存中是否存在该user实例的软引用，若有，从软引用中取得
        if (userReferences.containsKey(Constant.CACHE_USER)) {
            UserReference ref = (UserReference) userReferences.get(Constant.CACHE_USER);
            user = (UserInfo) ref.get();
        }

        //若没有软引用，或者从软引用中取得的实例是null，重新构建一个实例
        if (user == null) {
            SharedPreferences sharePre = context.getSharedPreferences(Constant.CACHE_USER, 0);
            user = new UserInfo();
            user.setPhoneNumber(sharePre.getString("phoneNumber", ""));
            user.setPassword(sharePre.getString("password", ""));
            user.setUserGuid(sharePre.getString("userGuid", ""));
            user.setName(sharePre.getString("name", ""));
            user.setOftenContactID(sharePre.getString("OftenContactID", ""));
            user.setWhetherLogin(sharePre.getBoolean("whetherLogin", false));
            user.setPreferArea(sharePre.getString("PreferArea", ""));
            user.setPreferSeat(sharePre.getString("PreferSeat", ""));
            user.setCardNo(sharePre.getString("CardNo", ""));
            this.cacheUser(user);
        }
        return user;
    }

    //清除所引用的user对象，已经被回收的UserReference对象
    public void clearCache() {
        UserReference ref = null;
        while ((ref = (UserReference) userQueue.poll()) != null) {
            userReferences.remove(ref._key);
        }
    }
}
