package com.microwintech.boardingtreasure.util;


import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.microwintech.boardingtreasure.ui.ManageApplication;

/**
 * SharePreference工具类
 *
 * @author Administrator
 */
public class PreferenceUtil {
    private static SharedPreferences mSharedPreferences;

    private static synchronized SharedPreferences getPreferneces() {
        if (mSharedPreferences == null) {
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ManageApplication.getInstance());
        }
        return mSharedPreferences;
    }

    public static void print() {
        System.out.println(getPreferneces().getAll());
    }

    /**
     * 保存String类型
     *
     * @param key
     * @param value
     */
    public static void putString(String key, String value) {
        getPreferneces().edit().putString(key, value).commit();
    }

    /**
     * 读取String类型
     *
     * @param key
     * @return
     */
    public static String getString(String key) {
        return getPreferneces().getString(key, "");

    }

    /**
     * 保存int类型
     *
     * @param key
     * @param value
     */
    public static void putInt(String key, int value) {
        getPreferneces().edit().putInt(key, value).commit();
    }

    /**
     * 读取int类型
     *
     * @param key
     * @return
     */
    public static int getInt(String key) {
        return getPreferneces().getInt(key, 0);
    }

    /**
     * 保存boolean类型
     *
     * @param key
     * @param value
     */
    public static void putBoolean(String key, Boolean value) {
        getPreferneces().edit().putBoolean(key, value).commit();
    }

    /**
     * 读取boolean类型
     *
     * @param key
     * @param defValue
     * @return
     */
    public static boolean getBoolean(String key, boolean defValue) {
        return getPreferneces().getBoolean(key, defValue);
    }

    /**
     * 保存long类型
     *
     * @param key
     * @param value
     */
    public static void putLong(String key, long value) {
        getPreferneces().edit().putLong(key, value).commit();
    }

    /**
     * 读取long类型
     *
     * @param key
     * @return
     */
    public static long getLong(String key) {
        return getPreferneces().getLong(key, 0);
    }

    /**
     * 清理全部数据
     */
    public static void clear() {
        getPreferneces().edit().clear().commit();
    }

    /**
     * 删除某个字段数据
     *
     * @param key
     */
    public static void removeString(String key) {
        getPreferneces().edit().remove(key).commit();
    }

}
