package com.microwintech.boardingtreasure.util;


import android.text.format.Time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
    private static String date_time_format = "yyyy-MM-dd HH:mm:ss";
    private static String date_time_without_sec_format = "yyyy-MM-dd HH:mm";
    private static String date_format = "yyyy-MM-dd";

    /**
     * 使用预设格式将字符串转为Date
     * 返回结果2013-01-24 10:07
     */
    public static String getDateTimeWithoutSecondNow() {
        SimpleDateFormat df = new SimpleDateFormat(date_time_without_sec_format);
        return df.format(new Date());
    }

    public static String getDateNow() {
        SimpleDateFormat df = new SimpleDateFormat(date_format);
        return df.format(new Date());
    }

    //返回结果2013-01-24 10:07:09
    public static String getDateTimeNow() {
        SimpleDateFormat df = new SimpleDateFormat(date_time_format);
        return df.format(new Date());
    }

    //返回结果2013-01-24 10:07:09
    public static String getDateTimeNow(long datetime) {
        Date cur_date = new Date(datetime);
        SimpleDateFormat df = new SimpleDateFormat(date_time_format);
        return df.format(cur_date);
    }

    public static String getDateNoTime(long datetime) {
        Date cur_date = new Date(datetime);
        SimpleDateFormat df = new SimpleDateFormat(date_format);
        return df.format(cur_date);
    }

    /**
     * 获得指定日期的前一天
     */
    public static String getSpecifiedDayBefore(String specifiedDay) {
        Calendar c = Calendar.getInstance();
        Date date = dateParse(specifiedDay);
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day - 1);
        String dayBefore = new SimpleDateFormat(date_format).format(c.getTime());
        return dayBefore;
    }

    /**
     * 获得指定日期的后一天
     */
    public static String getSpecifiedDayAfter(String specifiedDay) {
        Calendar c = Calendar.getInstance();
        Date date = dateParse(specifiedDay);
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day + 1);
        String dayAfter = new SimpleDateFormat(date_format).format(c.getTime());
        return dayAfter;
    }

    /**
     * 使用预设格式将字符串转为Date
     * 参数值  dateParse("2012-01-02")
     * 返回结果 Mon Jan 02 00:00:00 CST 2012
     */
    public static Date dateParse(String strDate) {
        if (strDate == null) {
            return new Date();
        }
        return parse(strDate, date_format);
    }

    public static Date dateParseNoTime(String strDate) {
        if (strDate == null) {
            return new Date();
        }
        return parse(strDate, date_format);
    }

    /**
     * 使用参数Format将字符串转为Date
     */
    public static Date parse(String strDate, String user_format) {
        SimpleDateFormat df = new SimpleDateFormat(user_format);
        try {
            return df.parse(strDate);
        } catch (ParseException e) {
            return null;
        }
    }

    public static int compareDiffYearNumber(String initialDate, String resultDate) {
        Date initDate = dateParseNoTime(initialDate);
        Date resuDate = dateParseNoTime(resultDate);
        int number = (int) (initDate.getTime() - resuDate.getTime()) / (1000 * 60 * 60 * 24);
        int count = number / 365;
        return count;
    }

    //比较日期相差的天数 2014-10-18  2014-10-22 相差 4 天
    //比较两天的大小 initialDate>resultDate  true
    public static boolean greaterDate(String initialDate, String resultDate) {
        //(int) ((anotherDate.getTime() - today.getTime()) / (1000 * 60 * 60 * 24)))
        Date initDate = dateParseNoTime(initialDate);
        Date resuDate = dateParseNoTime(resultDate);
        if (initDate.getTime() >= resuDate.getTime()) {
            return true;
        }
        return false;
    }

    // 根据出生年月日获取月份
    public static String getMonthDay(String birthday) {
        String month = "";
        try {
            month = birthday.substring(5, birthday.length());
            return month;
        } catch (Exception ex) {
        }
        return month;
    }


}