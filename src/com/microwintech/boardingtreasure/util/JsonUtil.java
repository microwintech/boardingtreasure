package com.microwintech.boardingtreasure.util;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Administrator on 2014/11/8.
 */
public class JsonUtil {
    public static String getValue(String json, String key) {
        try {
            JSONObject parObj = JSON.parseObject(json);
            return parObj.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
