package com.microwintech.boardingtreasure.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;
import com.microwintech.boardingtreasure.R;

/**
 * Created by Administrator on 2014/11/8.
 * 设备信息
 */
public class EquipmentUtil {
    private TelephonyManager telephonemanager;
    private String IMSI;
    private Context ctx;

    /**
     * 获取手机国际识别码IMEI
     */
    public EquipmentUtil(Context context) {
        ctx = context;
        telephonemanager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
    }

    /**
     * 获取手机号码
     */
    public String getNativePhoneNumber() {
        String phonenumber = telephonemanager.getLine1Number();
        return phonenumber;
    }

    /**
     * 获取手机服务商信息
     */
    public String getProvidersName() {
        String providerName = null;
        try {
            IMSI = telephonemanager.getSubscriberId();
            //IMSI前面三位460是国家号码，其次的两位是运营商代号，00、02是中国移动，01是联通，03是电信。
            System.out.print("IMSI是：" + IMSI);
            if (IMSI.startsWith("46000") || IMSI.startsWith("46002")) {
                providerName = "中国移动";
            } else if (IMSI.startsWith("46001")) {
                providerName = "中国联通";
            } else if (IMSI.startsWith("46003")) {
                providerName = "中国电信";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return providerName;
    }

    /**
     * 获取手机信息
     */
    public String getPhoneDeviceId() {
        TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getDeviceId();
    }

    public String getVersionName() {
        try {
            PackageManager packageManager = ctx.getPackageManager();
            // getPackageName()是你当前类的包名，0代表是获取版本信息
            PackageInfo packInfo = null;
            packInfo = packageManager.getPackageInfo(ctx.getPackageName(), 0);
            String version = packInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return ctx.getString(R.string.version_number);
        }

    }
}
