package com.microwintech.boardingtreasure.util;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * @author LuoYi
 *
 */
public class ToastUtil {
	private static Toast toast = null;
	public static int LENGTH_LONG = Toast.LENGTH_LONG;
	private static int LENGTH_SHORT = Toast.LENGTH_SHORT;

	public static void TextToast(Context context, CharSequence text, int duration) {
		toast = Toast.makeText(context, text, duration);
		toast.setGravity(Gravity.CENTER, 0, 300);
		toast.show();
	}
	public static void TextToast(Context context, int text, int duration) {
		toast = Toast.makeText(context, text, duration);
		toast.setGravity(Gravity.CENTER, 0, 300);
		toast.show();
	}

	public static void ImageToast(Context context, int ImageResourceId, CharSequence text, int duration) {
		toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 300);
		View toastView = toast.getView();
		ImageView img = new ImageView(context);
		img.setImageResource(ImageResourceId);
		LinearLayout ll = new LinearLayout(context);
		ll.addView(img);
		ll.addView(toastView);
		toast.setView(ll);
		toast.show();
	}
}