package com.microwintech.boardingtreasure.common.component;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.DatePicker;
import com.microwintech.boardingtreasure.R;

import java.util.Calendar;

/**
 * Created by Administrator on 2014/10/30.
 */
public class DatePickerFragment extends DialogFragment implements
        DatePickerDialog.OnDateSetListener {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dateDialog = new DatePickerDialog(getActivity(),R.style.date_dialog_theme, this, year, month, day);
        dateDialog.setInverseBackgroundForced(true);
        dateDialog.setCancelable(true);
        dateDialog.setCanceledOnTouchOutside(true);
        dateDialog.setButton(DialogInterface.BUTTON_POSITIVE, "确定",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Log.d("Picker", "Correct behavior!");
                    }
                });
        dateDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "取消",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Log.d("Picker", "Cancel!");
                    }
                });
        return dateDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
    }
}
