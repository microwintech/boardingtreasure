package com.microwintech.boardingtreasure.common.client;

import android.content.Context;
import com.alibaba.fastjson.JSON;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.common.security.SecurityUtil;
import com.microwintech.boardingtreasure.ui.LoadingDialog;

import java.sql.SQLOutput;
import java.util.Map;

/**
 * Created by Administrator on 2014/10/27.
 */
public class VolleyManage {
    private static class VolleyToolBuilder {
        static VolleyManage _instance = new VolleyManage();
    }

    public static VolleyManage getInstance() {
        return VolleyToolBuilder._instance;
    }

    public VolleyManage() {

    }

    private VolleyInterface volleyInterface;
    private LoadingDialog loadingDialog;
    private String content = null;

    public void methodPost(Context context, String content, String head, Map<String, String> mapParam, VolleyInterface volleyInterface) {
        this.content = content;
        if (null != content) {
            loadingDialog = new LoadingDialog(context, content);
            loadingDialog.show();
        }
        method(context, Request.Method.POST, head, mapParam, volleyInterface);
    }


    private void method(Context context, int method, String serverName, Map<String, String> mapParam, VolleyInterface serverInterface) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        this.volleyInterface = serverInterface;
        try {
            String url = Constant.API_URL + serverName;
            String jsonParam = JSON.toJSONString(mapParam, true);
            System.out.println("参数值：" + jsonParam);
            //统一加密成二进制
            final byte[] paramByte = SecurityUtil.encryptByPublicKey(context, jsonParam);

            VolleyClient request = new VolleyClient<BackInfo>(method, url, paramByte, BackInfo.class, new SuccessListener(), new WrongListener());
            requestQueue.add(request);
        } catch (Exception e) {

            if (null != content) {
                loadingDialog.dismiss();
            }
            VolleyBean bean = new VolleyBean();
            bean.setSuccess(false);
            bean.setContent("出现异常");
            serverInterface.gainData(bean);
        }
    }

    class SuccessListener implements Response.Listener<BackInfo> {

        @Override
        public void onResponse(BackInfo backInfo) {
            if (null != content) {
                loadingDialog.dismiss();
            }
            VolleyBean bean = new VolleyBean();
            if (null == backInfo) {
                // 返回值异常
                bean.setSuccess(false);
                bean.setMessage("返回值异常");
            } else {
                if ("true".equals(backInfo.getResult())) {
                    bean.setSuccess(true);

                    bean.setContent(backInfo.getMsg());
                } else {
                    bean.setSuccess(false);
                    bean.setContent(backInfo.getMsg());
                }

            }
            volleyInterface.gainData(bean);
        }
    }

    class WrongListener implements Response.ErrorListener {

        @Override
        public void onErrorResponse(VolleyError volleyError) {
            if (null != content) {
                loadingDialog.dismiss();
            }
            VolleyBean bean = new VolleyBean();
            bean.setSuccess(false);
            bean.setContent("出现错误");
            volleyInterface.gainData(bean);
        }
    }
}
