package com.microwintech.boardingtreasure.common.client;

import android.content.Context;
import android.os.AsyncTask;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.common.security.SecurityUtil;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/12/20.
 */
public class HttpPostManage {
    private static class HttpPostManageBuilder {
        static HttpPostManage _instance = new HttpPostManage();
    }

    public static HttpPostManage getInstance() {
        return HttpPostManageBuilder._instance;
    }

    public HttpPostManage() {

    }

    public VolleyBean methodPost(Context context, Map<String, Object> mapParam) {
        VolleyBean bean = new VolleyBean();
        try {
            // 请求的地址
            String spec = Constant.API_URL + Constant.BOARD_HANDLE_PASS_URL;
            // 根据地址创建URL对象
            URL url = new URL(spec);
            // 根据URL对象打开链接
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();
            // 设置请求的方式
            urlConnection.setRequestMethod("POST");
            // 设置请求的超时时间
            urlConnection.setReadTimeout(50000);
            urlConnection.setConnectTimeout(50000);
            JSONObject jsonObj = new JSONObject();
            for (String key : mapParam.keySet()) {
                jsonObj.put(key, mapParam.get(key));
            }
            // 传递的数据
            String jsonParam = JSON.toJSONString(mapParam, true);
            System.out.println("参数值：" + jsonParam);
            //统一加密成二进制
            final byte[] paramByte = SecurityUtil.encryptByPublicKey(context, jsonParam);
            // 设置请求的头
            urlConnection.setRequestProperty("Connection", "keep-alive");
            // 设置请求的头
            urlConnection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("Accept-Encoding", "");
            // 设置请求的头
            urlConnection.setRequestProperty("Content-Length",
                    String.valueOf(paramByte.length));
            urlConnection.setDoOutput(true); // 发送POST请求必须设置允许输出
            urlConnection.setDoInput(true); // 发送POST请求必须设置允许输入
            //获取输出流
            OutputStream os = urlConnection.getOutputStream();
            os.write(paramByte);
            os.flush();
            if (urlConnection.getResponseCode() == 200) {
                // 获取响应的输入流对象
                InputStream is = urlConnection.getInputStream();
                // 创建字节输出流对象
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                // 定义读取的长度
                int len = 0;
                // 定义缓冲区
                byte buffer[] = new byte[1024];
                // 按照缓冲区的大小，循环读取
                while ((len = is.read(buffer)) != -1) {
                    // 根据读取的长度写入到os对象中
                    baos.write(buffer, 0, len);
                }
                // 释放资源
                is.close();
                baos.close();
                // 返回字符串
                String result = new String(baos.toByteArray());
                JSONObject obj = JSON.parseObject(result);
                bean.setContent(obj.getString("msg"));
                if ("true".equals(obj.getString("result"))) {
                    bean.setSuccess(true);
                } else {
                    bean.setSuccess(false);
                }
            }
        } catch (Exception e) {
            bean.setSuccess(false);
            bean.setContent("服务器忙");
        }
        return bean;
    }

}
