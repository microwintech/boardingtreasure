package com.microwintech.boardingtreasure.common.client;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.microwintech.boardingtreasure.common.lang.Constant;

/**
 * Created by Administrator on 2014/10/26.
 */
public class ConnectionManage {
    public static String methodPost(String url_path, byte[] param) {
        HttpURLConnection connection = null;
        OutputStream os;
        InputStream is;
        try {
            URL url = new URL(Constant.API_URL + url_path);
            connection = (HttpURLConnection) url.openConnection();
            //设置输入输出流
            connection.setDoOutput(true);
            connection.setDoInput(true);
            //设置请求的方法
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(60000);
            //POST 方式不能缓存数据，则需要手动设置使用缓存的值为false
            connection.setUseCaches(false);
            os = connection.getOutputStream();
            //封装写给服务器的数据（要传递的参数）
            DataOutputStream dos = new DataOutputStream(os);
            dos.write(param);
            dos.close();
            /***读取服务器数据**/
            is = connection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            StringBuffer sb = new StringBuffer();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
        return null;
    }
}
