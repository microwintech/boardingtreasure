package com.microwintech.boardingtreasure.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;

/**
 * Created by Administrator on 2014/12/20.
 * 确认删除对话框
 */
public class ConfirmCancelDialog extends Dialog {
    private Context context;
    private ConfirmCancelInterface selectInterface;
    private String content;
    private boolean showNoCancel;

    public ConfirmCancelDialog(Context context, String content, boolean showNoCancel, ConfirmCancelInterface selectInterface) {
        super(context, R.style.Comfirm_Delete_Dialog);
        this.content = content;
        this.showNoCancel = showNoCancel;
        this.selectInterface = selectInterface;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.dialog_confirm_cancel);
        TextView contentTv = (TextView) findViewById(R.id.dialog_button_content_tv);
        contentTv.setText(content);
        Button confirmBtn = (Button) findViewById(R.id.dialog_button_confirm_btn);
        confirmBtn.setOnClickListener(new SelectClickListener(true));
        Button cancelBtn = (Button) findViewById(R.id.dialog_button_cancel_btn);
        if (showNoCancel) {
            cancelBtn.setVisibility(View.GONE);
        }
        cancelBtn.setOnClickListener(new SelectClickListener(false));
    }

    class SelectClickListener implements View.OnClickListener {
        boolean click;

        SelectClickListener(boolean click) {
            this.click = click;
        }

        @Override
        public void onClick(View v) {
            selectInterface.handleClick(click);
        }
    }
}
