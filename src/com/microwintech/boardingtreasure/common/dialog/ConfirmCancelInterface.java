package com.microwintech.boardingtreasure.common.dialog;

/**
 * Created by Administrator on 2014/12/20.
 */
public interface ConfirmCancelInterface {
    public void handleClick(boolean click);
}
