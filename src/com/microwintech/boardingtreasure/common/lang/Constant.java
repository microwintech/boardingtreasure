package com.microwintech.boardingtreasure.common.lang;

/**
 * Created by Administrator on 2014/10/23.
 */
public final class Constant {

    public static final String API_URL = "http://test.4008200098.com/WebService";
    //    http://test.4008200098.com/WebService/UserService.asmx/Login
    //手机获取验证码
    public static final String VERIFY_GET_VERIFICATION = "/UserService.asmx/GetVerification";
    //修改密码
    public static final String USER_UPDATE_PASSWORD = "/UserService.asmx/UpdatePassWord";

    //    按手机号码查询用户信息
    //
    public static final String USER_BY_MOBILE = "/UserService.asmx/GetUsersByMobile";
    //设备号获取用户信息
    public static final String USER_GET_USER_BY_DEVICE_NO = "/UserService.asmx/GetUserByDeviceNo";
    //用户通过手机注册
    public static final String USER_REGISTER_USER = "/UserService.asmx/RegisterUser";
    //用户新增
    public static final String USER_ADD_USER = "/UserService.asmx/addUser";
    //我的资料（完善）
    public static final String USER_UPDATE_OFTEN_CONTACT = "/UserService.asmx/updateOftenContacts";
    //
    //用户登陆
    public static final String USER_LOGIN_USER = "/UserService.asmx/Login";
    //查询常用联系人列表
    public static final String PASSENGER_OFTEN_CONTACT_LIST = "/UserService.asmx/GetOftenContactsList";
    //新增常用联系人
    public static final String USER_ADD_USE_CONTACT = "/UserService.asmx/addUser";
    //UserService.asmx/addUser?type=0

    public static final String MY_JOURNEY_TRIP_MESSAGE = "/BoardingPassService.asmx/GetTripMessage";

    //FlightService
//航班动态
    public static final int ACT_RES_FLIGHT_DYNAMIC_SETOUT_CITY_CODE = 70001;//出发返回
    public static final int ACT_RES_FLIGHT_DYNAMIC_ARRIVE_CITY_CODE = 70002;//到达返回
    //起降地查询
    public static final String FLIGHT_GET_FLIGHT_CITY = "/FlightService.asmx/GetFilghtByCity";
    //航班号查询
    public static final String FLIGHT_GET_FLIGHT_CODE = "/FlightService.asmx/GetFilghtByFlightCode";
    //我的登机牌
    public static final String MY_BOARDING_PASS_BY_USERGUID_CODE = "/BoardingPassService.asmx/GetBoardingPassByUserGuid";
    //    http://test.4008200098.com/WebService/FlightService.asmx/GetFilghtByFlightCode
    //http://test.4008200098.com/WebService/FlightService.asmx/GetFilghtByCity
//办理登机牌
    public static final String BOARD_HANDLE_PASS_URL = "/BoardingPassService.asmx/GetBoardingPass";
    //付款
    public static final String PAY_MENT_URL = "/BoardingPassService.asmx/PayMent";


    //办理登机牌
    public static final int ACT_RES_BOARDING_PASS_CHOOSE_CODE = 70003;//常用乘机人选择
    public static final int ACT_RES_BOARDING_ADDUP_PASS_CODE = 70004;//添加乘机人
    //用户缓存
    public static final String CACHE_USER = "USER_CACHE";
    public static final String LOGIN_REFRESH_ACTION_RECEIVER = "com.microwintech.boardingtreasure.ACTION.REFRESH.LOGIN";
}
