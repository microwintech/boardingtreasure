package com.microwintech.boardingtreasure.ui.passengermanage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2014/12/8.
 */
public class PassengerNewActivity extends Activity {
    private EditText nameEt;
    private EditText phoneEt;
    private EditText idCardEt;
    private UserInfo user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passenger_new);
        user = UserCache.getInstance().getUser(this);
        //取消
        TextView backTv = (TextView) findViewById(R.id.passenger_new_user_back_iv);
        backTv.setOnClickListener(new BackOnClickListener());
        nameEt = (EditText) findViewById(R.id.passenger_new_name_et);
        phoneEt = (EditText) findViewById(R.id.passenger_new_phone_et);
        idCardEt = (EditText) findViewById(R.id.passenger_new_idcard_number_et);
        EffectsButton saveEb = (EffectsButton) findViewById(R.id.passenger_new_save_eb);
        saveEb.setOnClickListener(new SaveOnClickListener());

    }

    class SaveOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            String name = nameEt.getText().toString();
            String phone = phoneEt.getText().toString();
            String isCard = idCardEt.getText().toString();
            Map<String, String> mapParam = new HashMap<String, String>();
            mapParam.put("UserGuid", user.getUserGuid());
            mapParam.put("Name", name);
            mapParam.put("Mobile", phone);
            mapParam.put("CardType", "身份证");
            mapParam.put("CardNo", isCard);
            mapParam.put("type", "0");
//            {
//                "UserGuid":"bd010d0a-c66b-4609-8c30-e4e58f19a3a2",
//                    "Name":"王振柱",
//                    "Mobile":"13818342667",
//                    "CardType":"身份证",
//                    "CardNo":"372426197502212015",
//                    "type"="0" 必填固定值0表示新增用户信息
//            }
            VolleyManage.getInstance().methodPost(PassengerNewActivity.this, "正在获取中...", Constant.USER_ADD_USE_CONTACT, mapParam, new SaveVolleyReal());
        }
    }

    class SaveVolleyReal implements VolleyInterface {

        @Override
        public void gainData(VolleyBean bean) {
            if (bean.isSuccess()) {
                Intent intent = new Intent();
                intent.putExtra("refresh", true);
                //把返回数据存入Intent
//                intent.putExtra("")
                setResult(RESULT_OK, intent);
                finish();
            } else {
                Toast.makeText(PassengerNewActivity.this, bean.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    class BackOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("refresh", false);
            //把返回数据存入Intent
//                intent.putExtra("")
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}