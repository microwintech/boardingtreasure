package com.microwintech.boardingtreasure.ui.passengermanage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.adapter.passengermanage.Passenger;
import com.microwintech.boardingtreasure.adapter.passengermanage.PassengerAdapter;
import com.microwintech.boardingtreasure.adapter.passengermanage.PassengerSelectInterface;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;
import com.microwintech.boardingtreasure.util.ToastUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/12/7.
 */
public class PassengerManageActivity extends Activity {
	private ListView passengerLv;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.passenger_manage);

		passengerLv = (ListView) findViewById(R.id.passenger_manage_lv);
		LinearLayout backLl = (LinearLayout) findViewById(R.id.passenger_manage_user_back_ll);
		backLl.setOnClickListener(new CreatePassenger('B'));
		LinearLayout newLl = (LinearLayout) findViewById(R.id.passenger_manage_user_add_ll);
		newLl.setOnClickListener(new CreatePassenger('N'));
		refreshData();

	}

	private void refreshData() {
		// 1-身份证 2-护照 3-港澳通行证 4-军人证
		Map<String, String> mapParam = new HashMap<String, String>();
		UserInfo userInfo = UserCache.getInstance().getUser(this);
		mapParam.put("UserGuid", userInfo.getUserGuid());
		VolleyManage.getInstance().methodPost(PassengerManageActivity.this, "正在获取中...", Constant.PASSENGER_OFTEN_CONTACT_LIST, mapParam, new PassengerResultListener());
	}

	class PassengerResultListener implements VolleyInterface {

		@Override
		public void gainData(VolleyBean bean) {
			if (bean.isSuccess()) {
				List<Passenger> passengerList = new ArrayList<Passenger>();
				JSONArray parentArray = JSON.parseArray(bean.getContent());
				int count = parentArray.size();
				for (int i = 0; i < count; i++) {
					JSONObject obj = parentArray.getJSONObject(i);
					Passenger p1 = new Passenger();
					p1.setUuid(obj.getString("UserGuid"));
					p1.setOftenContactId(obj.getString("OftenContactID"));
					p1.setName(obj.getString("Name"));
					p1.setPhoneNumber(obj.getString("Mobile"));
					p1.setCardName(obj.getString("CardType"));
					p1.setCardNumber(obj.getString("CardNo"));
					passengerList.add(p1);
				}
				PassengerAdapter adapter = new PassengerAdapter(PassengerManageActivity.this, passengerList, new SelectPassenger());
				passengerLv.setAdapter(adapter);
				// "UserGuid":"bd010d0a-c66b-4609-8c30-e4e58f19a3a2",
				// "OftenContactID":"bd010d0a-c66b-4609-8c30-e4e58f19a3a2",
				// "Name":"王振柱",
				// "Mobile":"13818342667",
				// "CardType":"身份证",
				// "CardNo":"372426197502212015",

			}
		}
	}

	// 编辑
	class SelectPassenger implements PassengerSelectInterface {

		@Override
		public void selectPassenger(Passenger passenger) {
			Intent intent = new Intent(PassengerManageActivity.this, PassengerEditActivity.class);
			intent.putExtra("userguid", passenger.getUuid());
			intent.putExtra("name", passenger.getName());
			intent.putExtra("phoneNumber", passenger.getPhoneNumber());
			intent.putExtra("cardNumber", passenger.getCardNumber());
			startActivity(intent);
		}
	}

	// 新增
	class CreatePassenger implements View.OnClickListener {

		char type;

		public CreatePassenger(char type) {
			this.type = type;
		}

		@Override
		public void onClick(View view) {
			switch (type) {
			case 'B':
				finish();
				break;

			case 'N':
				Intent intent = new Intent(PassengerManageActivity.this, PassengerNewActivity.class);
				intent.putExtra("userguid", "-1");
				intent.putExtra("name", "");
				intent.putExtra("phoneNumber", "");
				intent.putExtra("cardNumber", "");
				startActivityForResult(intent, 10000);
				break;

			default:
				break;
			}

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (10000 == requestCode && null != data) {
			boolean refresh = data.getBooleanExtra("refresh", false);
			if (refresh) {
				// 刷新
				refreshData();
			}
			Toast.makeText(PassengerManageActivity.this, "刷新", Toast.LENGTH_LONG).show();
		}
	}
}