package com.microwintech.boardingtreasure.ui.passengermanage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2014/12/8.
 */
public class PassengerEditActivity extends Activity {
    private EditText nameEt;
    private EditText phoneEt;
    private EditText idCardEt;
    private UserInfo user;
    private String oftenContactID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passenger_edit);
        
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String phoneNumber = intent.getStringExtra("phoneNumber");
        String cardNumber = intent.getStringExtra("cardNumber");
        oftenContactID = intent.getStringExtra("OftenContactID");
        user = UserCache.getInstance().getUser(this);
        //取消
        LinearLayout backLl = (LinearLayout) findViewById(R.id.passenger_edit_user_back_ll);
        backLl.setOnClickListener(new BackOnClickListener());
        nameEt = (EditText) findViewById(R.id.passenger_edit_name_et);
        nameEt.setText(name);
        phoneEt = (EditText) findViewById(R.id.passenger_edit_phone_et);
        phoneEt.setText(phoneNumber);
        idCardEt = (EditText) findViewById(R.id.passenger_edit_idcard_number_et);
        idCardEt.setText(cardNumber);
        EffectsButton saveEb = (EffectsButton) findViewById(R.id.passenger_edit_save_eb);
        saveEb.setOnClickListener(new SaveOnClickListener());
    }

    class SaveOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
//            "UserGuid":"bd010d0a-c66b-4609-8c30-e4e58f19a3a2",
//                    "OftenContactID":"bd010d0a-c66b-4609-8c30-e4e58f19a3a2",
//                    "Name":"王振柱",
//                    "Mobile":"13818342667",
//                    "CardType":"身份证",
//                    "CardNo":"372426197502212015",


            String name = nameEt.getText().toString();
            String phone = phoneEt.getText().toString();
            String isCard = idCardEt.getText().toString();
            Map<String, String> mapParam = new HashMap<String, String>();
            mapParam.put("UserGuid", user.getUserGuid());
            mapParam.put("Name", name);
            mapParam.put("Mobile", phone);
            mapParam.put("CardType", "身份证");
            mapParam.put("CardNo", isCard);
            mapParam.put("OftenContactID", oftenContactID);
//            {
//                "UserGuid":"bd010d0a-c66b-4609-8c30-e4e58f19a3a2",
//                    "Name":"王振柱",
//                    "Mobile":"13818342667",
//                    "CardType":"身份证",
//                    "CardNo":"372426197502212015",
//                    "type"="0" 必填固定值0表示新增用户信息
//            }
            VolleyManage.getInstance().methodPost(PassengerEditActivity.this, "正在获取中...", Constant.USER_UPDATE_OFTEN_CONTACT, mapParam, new EditVolleyReal());
        }
    }

    class EditVolleyReal implements VolleyInterface {

        @Override
        public void gainData(VolleyBean bean) {
            if (bean.isSuccess()) {
                Intent intent = new Intent();
                //把返回数据存入Intent
//                intent.putExtra("")
                setResult(RESULT_OK, intent);
                finish();
            } else {
                Toast.makeText(PassengerEditActivity.this, bean.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    class BackOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            finish();
        }
    }
}