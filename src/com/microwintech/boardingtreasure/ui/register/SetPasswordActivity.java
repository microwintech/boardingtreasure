package com.microwintech.boardingtreasure.ui.register;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;
import com.microwintech.boardingtreasure.ui.perfectinfomation.PerfectInfoActivity;
import com.microwintech.boardingtreasure.util.StringUtil;
import com.microwintech.boardingtreasure.util.ToastUtil;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2014/12/7.
 */
public class SetPasswordActivity extends Activity {
    private EditText inputPdEt, surePdEt;
    private String userguid, phoneNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_password);
        Intent intent = getIntent();
        userguid = intent.getStringExtra("userguid");
        phoneNumber = intent.getStringExtra("phoneNumber");
        inputPdEt = (EditText) findViewById(R.id.set_password_input_et);
        surePdEt = (EditText) findViewById(R.id.set_password_sure_et);
        EffectsButton nextStepEbtn = (EffectsButton) findViewById(R.id.set_password_next_step_btn);
        nextStepEbtn.setOnClickListener(new PassListener());
    }

    class PassListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            String inputPassword = inputPdEt.getText().toString();
            if (StringUtil.isBlank(inputPassword)) {
                ToastUtil.TextToast(SetPasswordActivity.this, "请您输入6位数密码", Toast.LENGTH_SHORT);
                return;
            }
            String surePassword = surePdEt.getText().toString();
            if (StringUtil.isBlank(surePassword)) {
                ToastUtil.TextToast(SetPasswordActivity.this, "请您输入确认密码", Toast.LENGTH_SHORT);
                return;
            }
            if (!inputPassword.equals(surePassword)) {
                ToastUtil.TextToast(SetPasswordActivity.this, "两次输入的密码不一致", Toast.LENGTH_SHORT);
            }
            Map<String, String> mapParam = new HashMap<String, String>();
            mapParam.put("UserGuid", userguid);
            mapParam.put("PassWord", inputPassword);
            VolleyManage.getInstance().methodPost(SetPasswordActivity.this, "正在处理中...", Constant.USER_UPDATE_PASSWORD, mapParam, new RegisterListener());
        }

        class RegisterListener implements VolleyInterface {

            @Override
            public void gainData(VolleyBean bean) {
                if (bean.isSuccess()) {
                    UserInfo user = new UserInfo();
                    user.setPhoneNumber(phoneNumber);
                    user.setPassword(inputPdEt.getText().toString());
                    user.setUserGuid(userguid);
                    user.setWhetherLogin(true);
                    UserCache.getInstance().addUser(SetPasswordActivity.this, user);
                    Intent intent = new Intent(SetPasswordActivity.this, PerfectInfoActivity.class);
                    intent.putExtra("userguid", userguid);
                    intent.putExtra("phoneNumber", phoneNumber);
                    intent.putExtra("type", "setpassword");
                    startActivity(intent);
                    finish();
                } else {
                    ToastUtil.TextToast(SetPasswordActivity.this, "系统忙，请您稍后再试！", Toast.LENGTH_SHORT);
                }
            }
        }
    }
}