package com.microwintech.boardingtreasure.ui.register;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.util.EquipmentUtil;
import com.microwintech.boardingtreasure.util.JsonUtil;
import com.microwintech.boardingtreasure.util.StringUtil;
import com.microwintech.boardingtreasure.util.ToastUtil;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2014/12/7.
 */
public class RegisterActivity extends Activity {
    EditText phoneNumberEt, verifyEt;//手机号，验证码
    private String verificationCode = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        phoneNumberEt = (EditText) findViewById(R.id.register_phone_number_et);
        EffectsButton verifyBtn = (EffectsButton) findViewById(R.id.register_verify_btn);
        verifyBtn.setOnClickListener(new VerifyClickListener('V'));
        verifyEt = (EditText) findViewById(R.id.register_verify_et);
        EffectsButton loginBtn = (EffectsButton) findViewById(R.id.register_next_step_btn);
        loginBtn.setOnClickListener(new VerifyClickListener('L'));
    }

    class VerifyClickListener implements View.OnClickListener {
        char type;

        VerifyClickListener(char type) {
            this.type = type;
        }

        @Override
        public void onClick(View view) {
            switch (type) {
                case 'V': {
                    String number = phoneNumberEt.getText().toString();
                    if (StringUtil.isNotBlank(number)) {
                        Map<String, String> mapParam = new HashMap<String, String>();
                        mapParam.put("MobilePhone", number);
                        VolleyManage.getInstance().methodPost(RegisterActivity.this, "正在获取中...", Constant.VERIFY_GET_VERIFICATION, mapParam, new VerifyListener());
                    } else {
                        ToastUtil.TextToast(RegisterActivity.this, "请您输入手机号", Toast.LENGTH_SHORT);
                    }
                    break;
                }
                case 'L': {
                    String verify = verifyEt.getText().toString();
                    if (StringUtil.isBlank(verify)) {
                        ToastUtil.TextToast(RegisterActivity.this, "请您输入验证码", Toast.LENGTH_SHORT);
                    } else {
                        if (verify.equals(verificationCode)) {
                            Map<String, String> mapParam = new HashMap<String, String>();
                            EquipmentUtil equipmentUtil = new EquipmentUtil(RegisterActivity.this);
                            //设备号
                            String deviceNo = equipmentUtil.getPhoneDeviceId();
                            //应用版本号
                            String version = equipmentUtil.getVersionName();
                            mapParam.put("MobilePhone", phoneNumberEt.getText().toString());
                            mapParam.put("DeviceNo", deviceNo);
                            mapParam.put("ControlID", "");
                            mapParam.put("Version", version);
                            mapParam.put("Verification", verify);
                            VolleyManage.getInstance().methodPost(RegisterActivity.this, "正在获取中...", Constant.USER_REGISTER_USER, mapParam, new RegisterListener());
                        } else {
                            ToastUtil.TextToast(RegisterActivity.this, "请您输入正确的验证码", Toast.LENGTH_SHORT);
                        }
                    }
                    break;
                }
            }
        }
    }

    class VerifyListener implements VolleyInterface {

        @Override
        public void gainData(VolleyBean bean) {
            if (bean.isSuccess()) {
                verificationCode = JsonUtil.getValue(bean.getContent(), "verificationCode");
            } else {
                ToastUtil.TextToast(RegisterActivity.this, bean.getMessage(), Toast.LENGTH_SHORT);

            }
        }
    }

    class RegisterListener implements VolleyInterface {

        @Override
        public void gainData(VolleyBean bean) {
            if (bean.isSuccess()) {
                String uuid = JsonUtil.getValue(bean.getContent(), "userguid");
                Intent intent = new Intent(RegisterActivity.this, SetPasswordActivity.class);
                intent.putExtra("userguid", uuid);
                intent.putExtra("phoneNumber", phoneNumberEt.getText().toString());
                startActivity(intent);
                finish();

            } else {
                ToastUtil.TextToast(RegisterActivity.this, bean.getMessage(), Toast.LENGTH_SHORT);
            }
        }
    }
}