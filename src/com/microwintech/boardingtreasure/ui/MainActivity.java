package com.microwintech.boardingtreasure.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.microwintech.boardingtreasure.R;

/**
 * Created by Administrator on 2014/11/10.
 */
public class MainActivity extends Activity implements View.OnClickListener {
    private HomeFragment homeFragment;
    private MyPeonyFragment myAoLianFragment;
    private ContactFragment contactFragment;
    /**
     * 用于对Fragment进行管理
     */
    private FragmentManager fragmentManager;
    private ImageButton homeIb, myAoLianIb, contactAoLianIb;
	private TextView homeTv;
	private TextView myaolianTv;
	private TextView contactaolianTv;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);
        
        
        fragmentManager = getFragmentManager();
        LinearLayout homeLl = (LinearLayout) findViewById(R.id.main_tab_home_ll);
        LinearLayout myAoLianRl = (LinearLayout) findViewById(R.id.main_tab_myaolian_ll);
        LinearLayout contactRl = (LinearLayout) findViewById(R.id.main_tab_contactaolian_ll);
        homeLl.setOnClickListener(this);
        myAoLianRl.setOnClickListener(this);
        contactRl.setOnClickListener(this);
        homeIb = (ImageButton) findViewById(R.id.main_tab_home_ib);
        myAoLianIb = (ImageButton) findViewById(R.id.main_tab_myaolian_ib);
        contactAoLianIb = (ImageButton) findViewById(R.id.main_tab_contactaolian_ib);
        
        homeTv = (TextView) findViewById(R.id.main_tab_home_tv);
        myaolianTv = (TextView) findViewById(R.id.main_tab_myaolian_tv);
        contactaolianTv = (TextView) findViewById(R.id.main_tab_contactaolian_tv);
        setTabSelection(0);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_tab_home_ll: {
                setTabSelection(0);
                break;
            }
            case R.id.main_tab_myaolian_ll: {
                setTabSelection(1);
                break;
            }
            case R.id.main_tab_contactaolian_ll: {
                setTabSelection(2);
                break;
            }
        }
    }

    /**
     * 根据传入的index参数来设置选中的tab页。
     */
    private void setTabSelection(int index) {
        //  清除掉所有的选中状态。
        homeIb.setImageResource(R.drawable.main_tab_home_nor);
        myAoLianIb.setImageResource(R.drawable.main_tab_myaolian_nor);
        contactAoLianIb.setImageResource(R.drawable.main_tab_contactaolian_nor);
        
        homeTv.setTextColor(this.getResources().getColor(R.color.deep_gainsboro));
        myaolianTv.setTextColor(this.getResources().getColor(R.color.deep_gainsboro));
        contactaolianTv.setTextColor(this.getResources().getColor(R.color.deep_gainsboro));
        
        // 开启一个Fragment事务
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        // 先隐藏掉所有的Fragment，以防止有多个Fragment显示在界面上的情况
        hideFragments(transaction);
        switch (index) {
            case 0: {
                // 当点击了消息tab时，改变控件的图片和文字颜色
                homeIb.setImageResource(R.drawable.main_tab_home_press);
                homeTv.setTextColor(this.getResources().getColor(R.color.deep_bule));
//                homeFragment = new HomeFragment();
//                transaction.replace(R.id.fragment_content, homeFragment);
                if (homeFragment == null) {
                    // 如果MessageFragment为空，则创建一个并添加到界面上
                    homeFragment = new HomeFragment();
                    transaction.add(R.id.fragment_content, homeFragment);
                } else {
                    // 如果MessageFragment不为空，则直接将它显示出来
                    transaction.show(homeFragment);
                }
                break;
            }
            case 1: {
                // 当点击了消息tab时，改变控件的图片和文字颜色
                myAoLianIb.setImageResource(R.drawable.main_tab_myaolian_press);
                myaolianTv.setTextColor(this.getResources().getColor(R.color.deep_bule));
//                myAoLianFragment = new MyPeonyFragment();
//                transaction.replace(R.id.fragment_content, myAoLianFragment);
                if (myAoLianFragment == null) {
                    // 如果MessageFragment为空，则创建一个并添加到界面上
                    myAoLianFragment = new MyPeonyFragment();
                    transaction.add(R.id.fragment_content, myAoLianFragment);
                } else {
                    // 如果MessageFragment不为空，则直接将它显示出来
                    transaction.show(myAoLianFragment);
                }
                break;
            }
            case 2: {
                // 当点击了动态tab时，改变控件的图片和文字颜色
                contactAoLianIb.setImageResource(R.drawable.main_tab_contactaolian_press);
                contactaolianTv.setTextColor(this.getResources().getColor(R.color.deep_bule));
//                contactFragment = new ContactFragment();
//                transaction.replace(R.id.fragment_content, contactFragment);
                if (contactFragment == null) {
                    // 如果NewsFragment为空，则创建一个并添加到界面上
                    contactFragment = new ContactFragment();
                    transaction.add(R.id.fragment_content, contactFragment);
                } else {
                    // 如果NewsFragment不为空，则直接将它显示出来
                    transaction.show(contactFragment);
                }
                break;
            }
        }
        transaction.commit();
    }


    /**
     * 将所有的Fragment都置为隐藏状态。
     *
     * @param transaction 用于对Fragment执行操作的事务
     */
    private void hideFragments(FragmentTransaction transaction) {
        if (homeFragment != null) {
            transaction.hide(homeFragment);
        }
        if (myAoLianFragment != null) {
            transaction.hide(myAoLianFragment);
        }
        if (contactFragment != null) {
            transaction.hide(contactFragment);
        }


    }

    public void switchContent(Fragment fragment) {
        homeFragment = null;
        myAoLianFragment = null;
        contactFragment = null;
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_content, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}