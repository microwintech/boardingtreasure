package com.microwintech.boardingtreasure.ui.perfectinfomation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;
import com.microwintech.boardingtreasure.ui.HomeFragment;
import com.microwintech.boardingtreasure.util.ToastUtil;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2014/12/7.
 */
public class PerfectInfoActivity extends Activity {
    private String userguid, phoneNumber;
    private EditText nameEt, phoneEt, idCardNumberEt;//姓名，电话，证件号
    private RadioGroup areaRg, typeRg;
    private String type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfect_info);
        
        Intent intent = getIntent();
        userguid = intent.getStringExtra("userguid");
        phoneNumber = intent.getStringExtra("phoneNumber");
        type = intent.getStringExtra("type");
        nameEt = (EditText) findViewById(R.id.perfect_name_et);
        phoneEt = (EditText) findViewById(R.id.perfect_info_phone_et);
        idCardNumberEt = (EditText) findViewById(R.id.perfect_idcard_number_et);
        areaRg = (RadioGroup) findViewById(R.id.perfect_info_seat_area_rg);
        typeRg = (RadioGroup) findViewById(R.id.perfect_info_seat_type_rg);
        EffectsButton finishEb = (EffectsButton) findViewById(R.id.perfect_info_finish_btn);
        finishEb.setOnClickListener(new FinishClickListener());
        if ("setpassword".equals(type)) {
            phoneEt.setText(phoneNumber);
        }
        //1-身份证 2-护照 3-港澳通行证 4-军人证
        if ("my".equals(type)) {
            //点击我的资料
            Map<String, String> mapParam = new HashMap<String, String>();
            mapParam.put("MobilePhone", phoneNumber);
            VolleyManage.getInstance().methodPost(PerfectInfoActivity.this, "正在获取中...", Constant.USER_BY_MOBILE, mapParam, new UserInfoVolleyRealize());
        }


    }

    class UserInfoVolleyRealize implements VolleyInterface {
        @Override
        public void gainData(VolleyBean bean) {
            if (bean.isSuccess()) {
                JSONObject parJson = JSON.parseObject(bean.getContent());
                userguid = parJson.getString("UserGuid");
                nameEt.setText(parJson.getString("Name"));
                phoneNumber = parJson.getString("MobilePhone");
                phoneEt.setText(parJson.getString("MobilePhone"));
                idCardNumberEt.setText(parJson.getString("CardNo"));
                //CardType
            }
        }
    }

    class FinishClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Map<String, String> mapParam = new HashMap<String, String>();
            mapParam.put("UserGuid", userguid);
            mapParam.put("Name", nameEt.getText().toString());
            mapParam.put("Mobile", phoneEt.getText().toString());
            mapParam.put("CardType", "身份证");
            mapParam.put("CardNo", idCardNumberEt.getText().toString());
            mapParam.put("PreferArea", gainSeatArea());
            mapParam.put("PreferSeat", gainSeatType());
            if ("setpassword".equals(type)) {
                mapParam.put("type", "1");
                //设置用户密码，就是新增
                VolleyManage.getInstance().methodPost(PerfectInfoActivity.this, "正在获取中...", Constant.USER_ADD_USER, mapParam, new AddVolleyRealize());
            }
            if ("my".equals(type)) {
                //我的资料，就是修改
                UserInfo user=UserCache.getInstance().getUser(PerfectInfoActivity.this);
                mapParam.put("OftenContactID",user.getOftenContactID());
                VolleyManage.getInstance().methodPost(PerfectInfoActivity.this, "正在获取中...", Constant.USER_UPDATE_OFTEN_CONTACT, mapParam, new FinishVolleyRealize());
            }

        }
    }

    private String gainSeatArea() {
        String area = "";
        switch (areaRg.getCheckedRadioButtonId()) {
            case R.id.perfect_area_qian_rb: {
                area = "前排";
                break;
            }
            case R.id.perfect_area_zhong_rb: {
                area = "中间";
                break;
            }
            case R.id.perfect_area_hou_rb: {
                area = "后排";
                break;
            }

        }
        return area;
    }


    private String gainSeatType() {
        String area = "";
        switch (typeRg.getCheckedRadioButtonId()) {
            case R.id.perfect_type_chuang_rb: {
                area = "靠窗";
                break;
            }
            case R.id.perfect_type_dao_rb: {
                area = "过道";
                break;
            }
            case R.id.perfect_type_all_rb: {
                area = "均可";
                break;
            }

        }
        return area;
    }

    class FinishVolleyRealize implements VolleyInterface {
        @Override
        public void gainData(VolleyBean bean) {
            if (bean.isSuccess()) {

                finish();
            } else {
                ToastUtil.TextToast(PerfectInfoActivity.this, "系统忙，请您稍后再试！", Toast.LENGTH_SHORT);
            }
        }
    }

    class AddVolleyRealize implements VolleyInterface {
        @Override
        public void gainData(VolleyBean bean) {
            //新增
            if (bean.isSuccess()) {
                UserInfo info = new UserInfo();
                String oftenContactId = JSON.parseObject(bean.getContent()).getString("OftenContactID");
                info.setOftenContactID(oftenContactId);
                info.setCardNo(idCardNumberEt.getText().toString());
                info.setPreferArea(gainSeatArea());
                info.setPreferSeat(gainSeatType());
                UserCache.getInstance().modifyUser(PerfectInfoActivity.this, info);
                finish();
            } else {
                ToastUtil.TextToast(PerfectInfoActivity.this, "系统忙，请您稍后再试！", Toast.LENGTH_SHORT);
            }
        }
    }
}