package com.microwintech.boardingtreasure.ui.boardpass;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.adapter.passengermanage.Passenger;
import com.microwintech.boardingtreasure.adapter.passengermanage.PassengerSelectInterface;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;
import com.microwintech.platform.pay.PayDemoActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/11/30.
 * 乘机人选择
 */
public class PassengerChooseActivity extends Activity {
    private ListView chooseLv;
    private String passengerJson;
    private List<Passenger> passengerList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passenger_choose);
        
        Intent intent = getIntent();
        passengerJson = intent.getStringExtra("passengerJson");
        UserInfo user = UserCache.getInstance().getUser(PassengerChooseActivity.this);
        chooseLv = (ListView) findViewById(R.id.passenger_choose_contact_lv);
        LinearLayout backLl = (LinearLayout) findViewById(R.id.common_top_back_ll);
        EffectsButton sureEb = (EffectsButton) findViewById(R.id.passenger_chose_sure_eb);

        sureEb.setOnClickListener(new SureClickListener());
        backLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Map<String, String> mapParam = new HashMap<String, String>();
        mapParam.put("UserGuid", user.getUserGuid());
        VolleyManage.getInstance().methodPost(PassengerChooseActivity.this, "正在获取中...", Constant.PASSENGER_OFTEN_CONTACT_LIST, mapParam, new ChooseVolleyReal());
    }

    class ChooseVolleyReal implements VolleyInterface {

        @Override
        public void gainData(VolleyBean bean) {
            passengerList = new ArrayList<Passenger>();
            if (bean.isSuccess()) {
                JSONArray parentArray = JSON.parseArray(bean.getContent());
                int count = parentArray.size();
                for (int i = 0; i < count; i++) {
                    JSONObject obj = parentArray.getJSONObject(i);
                    Passenger passenger = new Passenger();
                    passenger.setUuid(obj.getString("UserGuid"));
                    passenger.setOftenContactId(obj.getString("OftenContactID"));
                    passenger.setName(obj.getString("Name"));
                    passenger.setPhoneNumber(obj.getString("Mobile"));
                    passenger.setCardName(obj.getString("CardType"));
                    String cardNo = obj.getString("CardNo");
                    if (passengerJson.contains(cardNo)) {
                        passenger.setWhetherSelect(true);
                    } else {
                        passenger.setWhetherSelect(false);
                    }
                    passenger.setCardNumber(obj.getString("CardNo"));
//                    private String preferArea;
//                    private String preferSeat;
                    passengerList.add(passenger);
                }
                PassengerChooseAdpter adapter = new PassengerChooseAdpter(PassengerChooseActivity.this, passengerList, new ChooseSelectPassenger());
                chooseLv.setAdapter(adapter);
                chooseLv.setItemsCanFocus(false);
                chooseLv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            }
        }
    }

    class ChooseSelectPassenger implements PassengerSelectInterface {
        @Override
        public void selectPassenger(Passenger passenger) {
            Intent intent = new Intent(PassengerChooseActivity.this, BoardingPassActivity.class);
            intent.putExtra("name", passenger.getName());
            intent.putExtra("phone", passenger.getPhoneNumber());
            intent.putExtra("cardNumber", passenger.getCardNumber());
            intent.putExtra("PreferArea", passenger.getPreferArea());
            intent.putExtra("PreferSeat", passenger.getPreferSeat());
            intent.putExtra("Status", "False");
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    class SureClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            JSONArray array = new JSONArray();
            for (Passenger pass : passengerList) {
                if (pass.isWhetherSelect()) {
                    JSONObject obj = new JSONObject();
                    obj.put("name", pass.getName());
                    obj.put("phone", pass.getPhoneNumber());
                    obj.put("cardNumber", pass.getCardNumber());
                    obj.put("PreferArea", pass.getPreferArea());
                    obj.put("PreferSeat", pass.getPreferSeat());
                    obj.put("Status", "False");
                    array.add(obj);
                }
            }
            if (array.size() < 1) {
                Toast.makeText(PassengerChooseActivity.this, "请您选择乘机人", Toast.LENGTH_LONG).show();

            } else {
                Intent intent = new Intent(PassengerChooseActivity.this, BoardingPassActivity.class);
                intent.putExtra("passengerArray", array.toJSONString());
                setResult(RESULT_OK, intent);
                finish();
            }

//            List<Passenger> passengerList = new ArrayList<Passenger>();


        }
    }
}