package com.microwintech.boardingtreasure.ui.boardpass;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;
import com.microwintech.boardingtreasure.util.StringUtil;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2014/11/30.
 */
public class AddUpPassengerActivity extends Activity {
    private RadioGroup areaRg, typeRg;
    private EditText nameEt, phoneEt, idCardNumberEt;//姓名，电话，证件号
    private UserInfo user;
    private CheckBox addupCb;
    private String passengerJson;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_up_passenger);
        
        Intent intent = getIntent();
        passengerJson = intent.getStringExtra("passengerJson");
        user = UserCache.getInstance().getUser(this);
        nameEt = (EditText) findViewById(R.id.add_up_name_et);
        phoneEt = (EditText) findViewById(R.id.add_up_phone_et);
        idCardNumberEt = (EditText) findViewById(R.id.perfect_idcard_number_et);
        areaRg = (RadioGroup) findViewById(R.id.add_up_seat_area_rg);
        typeRg = (RadioGroup) findViewById(R.id.add_up_seat_type_rg);
        addupCb = (CheckBox) findViewById(R.id.add_up_choose_cb);
        EffectsButton addUp = (EffectsButton) findViewById(R.id.add_up_finish_btn);
        addUp.setOnClickListener(new SaveOnClickListener());
        
        LinearLayout backLl = (LinearLayout) findViewById(R.id.common_top_back_ll);
        backLl.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
    }


    class SaveOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            String name = nameEt.getText().toString();
            String phone = phoneEt.getText().toString();
            String isCard = idCardNumberEt.getText().toString();
            if (StringUtil.isBlank(name)) {
                Toast.makeText(AddUpPassengerActivity.this, "请输入姓名", Toast.LENGTH_LONG).show();
                return;
            }

            if (StringUtil.isBlank(phone)) {
                Toast.makeText(AddUpPassengerActivity.this, "请输入手机号", Toast.LENGTH_LONG).show();
                return;
            }
            if (StringUtil.isBlank(isCard)) {
                Toast.makeText(AddUpPassengerActivity.this, "请输入身份证", Toast.LENGTH_LONG).show();
                return;
            }
            if (passengerJson.contains(isCard)) {
                Toast.makeText(AddUpPassengerActivity.this, "此人的已经", Toast.LENGTH_LONG).show();
                return;
            }


            if (addupCb.isChecked()) {

                Map<String, String> mapParam = new HashMap<String, String>();
                mapParam.put("UserGuid", user.getUserGuid());
                mapParam.put("type", "1");//0 表示常用联系人，1 表示信息
                mapParam.put("Name", name);
                mapParam.put("Mobile", phone);
                mapParam.put("CardType", "身份证");
                mapParam.put("CardNo", isCard);
                mapParam.put("PreferArea", gainSeatArea());
                mapParam.put("PreferSeat", gainSeatType());
                VolleyManage.getInstance().methodPost(AddUpPassengerActivity.this, "正在获取中...", Constant.USER_ADD_USE_CONTACT, mapParam, new SaveVolleyReal());
            } else {
                finishIntent();
            }

//            {
//                "UserGuid":"bd010d0a-c66b-4609-8c30-e4e58f19a3a2",
//                    "Name":"王振柱",
//                    "Mobile":"13818342667",
//                    "CardType":"身份证",
//                    "CardNo":"372426197502212015",
//                    "type"="0" 必填固定值0表示新增用户信息
//            }

        }
    }

    class SaveVolleyReal implements VolleyInterface {

        @Override
        public void gainData(VolleyBean bean) {
            if (bean.isSuccess()) {
                finishIntent();
            } else {
                Toast.makeText(AddUpPassengerActivity.this, bean.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private String gainSeatArea() {
        String area = "";
        switch (areaRg.getCheckedRadioButtonId()) {
            case R.id.perfect_area_qian_rb: {
                area = "前排";
                break;
            }
            case R.id.perfect_area_zhong_rb: {
                area = "中间";
                break;
            }
            case R.id.perfect_area_hou_rb: {
                area = "后排";
                break;
            }

        }
        return area;
    }


    private String gainSeatType() {
        String area = "";
        switch (typeRg.getCheckedRadioButtonId()) {
            case R.id.perfect_type_chuang_rb: {
                area = "靠窗";
                break;
            }
            case R.id.perfect_type_dao_rb: {
                area = "过道";
                break;
            }
            case R.id.perfect_type_all_rb: {
                area = "均可";
                break;
            }
        }
        return area;
    }

    class BackOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            finish();
        }
    }

    private void finishIntent() {
        Intent intent = new Intent(AddUpPassengerActivity.this, BoardingPassActivity.class);
        intent.putExtra("name", nameEt.getText().toString());
        intent.putExtra("phone", phoneEt.getText().toString());
        intent.putExtra("cardNumber", idCardNumberEt.getText().toString());
        intent.putExtra("PreferArea", gainSeatArea());
        intent.putExtra("PreferSeat", gainSeatType());
        if (addupCb.isChecked()) {
            intent.putExtra("Status", "False");
        } else {
            intent.putExtra("Status", "True");
        }
        setResult(RESULT_OK, intent);
        finish();
    }
}