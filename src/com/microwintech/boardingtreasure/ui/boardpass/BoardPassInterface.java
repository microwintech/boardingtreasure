package com.microwintech.boardingtreasure.ui.boardpass;

import com.microwintech.boardingtreasure.adapter.boardpass.BoardPassenger;
import com.microwintech.boardingtreasure.adapter.boardpass.BoardResult;

/**
 * Created by Administrator on 2014/12/20.
 */
public interface BoardPassInterface {
    public void gainBoardResult(BoardPassenger board);
}
