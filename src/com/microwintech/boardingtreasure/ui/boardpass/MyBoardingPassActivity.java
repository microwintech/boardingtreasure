package com.microwintech.boardingtreasure.ui.boardpass;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.adapter.boardpass.MyBoardPass;
import com.microwintech.boardingtreasure.adapter.boardpass.MyBoardPassAdapter;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/11/30.
 * 我的登机牌
 */
public class MyBoardingPassActivity extends Activity {
    private TextView numberTv;
    private ListView boardingLv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_boarding_pass);
        UserInfo userInfo = UserCache.getInstance().getUser(this);
        boardingLv = (ListView) findViewById(R.id.my_boarding_pass_lv);
        numberTv = (TextView) findViewById(R.id.my_board_pass_number_tv);
        Map<String, String> mapParam = new HashMap<String, String>();
        mapParam.put("UserGuid", userInfo.getUserGuid());
        VolleyManage.getInstance().methodPost(MyBoardingPassActivity.this, "正在获取中...", Constant.MY_BOARDING_PASS_BY_USERGUID_CODE, mapParam, new BoardVolleyReal());
    }

    class BoardVolleyReal implements VolleyInterface {

        @Override
        public void gainData(VolleyBean bean) {
            List<MyBoardPass> boardList = new ArrayList<MyBoardPass>();
            if (bean.isSuccess()) {

                JSONArray parArray = JSON.parseArray(bean.getContent());
                int count = parArray.size();
                for (int i = 0; i < count; i++) {
                    JSONObject jsonObj = parArray.getJSONObject(i);
                    MyBoardPass board = new MyBoardPass();
                    board.setUserName(jsonObj.getString("UserName"));
                    board.setFlightCode(jsonObj.getString("FlightCode"));
                    board.setFlightDate(jsonObj.getString("FlightDate"));
                    board.setDepAirport(jsonObj.getString("DepAirport"));
                    board.setArrAirport(jsonObj.getString("ArrAirport"));
                    board.setStatus(jsonObj.getString("Status"));
                    board.setOrderId(jsonObj.getString("OrderID"));
                    boardList.add(board);
                }

//                "UserName": "\u5353\u73b2\u73b2",
//                        "FlightCode": "CA1936",
//                        "FlightDate": "2014-11-28",
//                        "DepAirport": "\u5e7f\u5dde\u767d\u4e91",
//                        "ArrAirport": "\u4e0a\u6d77\u6d66\u4e1c",
//                        "Status": "\u529e\u7406\u4e2d"

            }

            MyBoardPass board = new MyBoardPass();
            board.setUserName("张三");
            board.setFlightCode("MU5415");
            board.setFlightDate("2014-11-22");
            board.setDepAirport("上海虹桥");
            board.setArrAirport("北京首都");
            board.setStatus("办理中");
            boardList.add(board);
            board = new MyBoardPass();
            board.setUserName("张三");
            board.setFlightCode("MU5413");
            board.setFlightDate("2014-10-22");
            board.setDepAirport("上海虹桥");
            board.setArrAirport("北京首都");
            board.setStatus("待付款");
            boardList.add(board);
            numberTv.setText(String.valueOf(boardList.size()));
            MyBoardPassAdapter boardAdapter = new MyBoardPassAdapter(MyBoardingPassActivity.this, boardList);
            boardingLv.setAdapter(boardAdapter);
        }
    }
}