package com.microwintech.boardingtreasure.ui.boardpass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.adapter.passengermanage.Passenger;
import com.microwintech.boardingtreasure.adapter.passengermanage.PassengerSelectInterface;

import java.util.List;

/**
 * Created by Administrator on 2014/12/17.
 */
public class PassengerChooseAdpter extends BaseAdapter {
    private Context context;
    private List<Passenger> passengerList;
    private LayoutInflater layoutInflater;
    private PassengerSelectInterface passengerInterface;

    public PassengerChooseAdpter(Context context, List<Passenger> passengerList, PassengerSelectInterface passengerInterface) {
        this.context = context;
        this.passengerList = passengerList;
        this.passengerInterface = passengerInterface;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return passengerList.size();
    }

    @Override
    public Object getItem(int i) {
        return passengerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.passenger_choose_item, null);
            holder = new ViewHolder();
            holder.chooseCb = (CheckBox) view.findViewById(R.id.passenger_choose_cb);
            holder.nameTv = (TextView) view.findViewById(R.id.passenger_choose_item_name_tv);
            holder.phoneTv = (TextView) view.findViewById(R.id.passenger_choose_item_phone_tv);
            holder.cardTv = (TextView) view.findViewById(R.id.passenger_choose_item_card_tv);
            holder.numberTv = (TextView) view.findViewById(R.id.passenger_choose_item_number_tv);
            holder.chooseCb.setTag(i);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Passenger passenger = passengerList.get(i);
        holder.nameTv.setText(passenger.getName());
        holder.phoneTv.setText(passenger.getPhoneNumber());
        holder.cardTv.setText(passenger.getCardName());
        holder.numberTv.setText(passenger.getCardNumber());
        holder.chooseCb.setOnCheckedChangeListener(new CheckChangeListener(i));
        holder.chooseCb.setChecked(passenger.isWhetherSelect());
//        holder.numberTv.setOnClickListener(new EditClickListener(passenger));
        return view;
    }

    class CheckChangeListener implements CompoundButton.OnCheckedChangeListener {
        int position;

        CheckChangeListener(int position) {
            this.position = position;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                passengerList.get(position).setWhetherSelect(true);
            } else {
                passengerList.get(position).setWhetherSelect(false);
            }
        }
    }

    class EditClickListener implements View.OnClickListener {
        Passenger passenger;

        EditClickListener(Passenger passenger) {
            this.passenger = passenger;
        }

        @Override
        public void onClick(View view) {
            passengerInterface.selectPassenger(passenger);
        }
    }

    class ViewHolder {
        CheckBox chooseCb;
        TextView nameTv;
        TextView phoneTv;
        TextView cardTv;
        TextView numberTv;

    }
}
