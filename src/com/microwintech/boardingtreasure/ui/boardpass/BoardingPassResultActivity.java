package com.microwintech.boardingtreasure.ui.boardpass;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.adapter.boardpass.BoardResult;
import com.microwintech.boardingtreasure.adapter.boardpass.BoardResultAdapter;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.ui.onlinepayment.OnlinePaymentActivity;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/11/30.
 * 办理登机牌提交成功
 */
public class BoardingPassResultActivity extends Activity {
    private ListView resultLv;
    private String orderID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boarding_pass_result);
        Intent intent = getIntent();
        String passengerJSONArray = intent.getStringExtra("passengerJSONArray");
        String orderJson = intent.getStringExtra("orderJson");
        String flightNumber = intent.getStringExtra("flight_number");
        String date = intent.getStringExtra("flight_date");

        TextView flightNumberTv = (TextView) findViewById(R.id.borad_result_flight_number_tv);
        TextView flightDateTv = (TextView) findViewById(R.id.borad_result_flight_date_tv);
        TextView airPortTv = (TextView) findViewById(R.id.borad_result_flight_airport_tv);
        resultLv = (ListView) findViewById(R.id.board_pass_result_lv);
        EffectsButton payEb = (EffectsButton) findViewById(R.id.boarding_pass_pay_btn);
        payEb.setOnClickListener(new PayOnClickListener());
        flightNumberTv.setText(flightNumber);
        flightDateTv.setText(date);
        JSONObject orderObj = JSON.parseObject(orderJson);
        orderID = orderObj.getString("OrderID");
        String airport = orderObj.getString("Airport");
        String needPayMent = orderObj.getString("NeedPayMent");
        if (needPayMent.toUpperCase().equals("TRUE")) {
            payEb.setVisibility(View.VISIBLE);
        } else {
            payEb.setVisibility(View.GONE);
        }
//        intent.putExtra("passengerJSONArray", parArray.toJSONString());
//        intent.putExtra("orderJson", "{\"OrderID\":\"12345\",\"CityOpenStatus\":\"True\",\"NeedPayMent\":\"True\",\"Airport\":\"上海虹桥-北京首都\"}");
        airPortTv.setText(airport);
        refreshData(passengerJSONArray);
//        Map<String, String> mapParam = new HashMap<String, String>();
//        mapParam.put("UserGuid", "");
//        VolleyManage.getInstance().methodPost(BoardingPassResultActivity.this, "正在获取中...", Constant.VERIFY_GET_VERIFICATION, mapParam, new ResultVolleyReal());
    }


    private void refreshData(String jsonStr) {
        List<BoardResult> boardList = new ArrayList<BoardResult>();
        JSONArray parArray = JSON.parseArray(jsonStr);
        for (int i = 0; i < parArray.size(); i++) {
            JSONObject obj = parArray.getJSONObject(i);
            BoardResult board = new BoardResult();
            board.setName(obj.getString("UserName"));
            board.setPhone(obj.getString("MobilePhone"));
            board.setCardNumber(obj.getString("CardNo"));
            boardList.add(board);
        }

        BoardResultAdapter resultAdapter = new BoardResultAdapter(BoardingPassResultActivity.this, boardList);
        resultLv.setAdapter(resultAdapter);
    }


    class PayOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(BoardingPassResultActivity.this, OnlinePaymentActivity.class);
            startActivity(intent);
        }
    }
}