package com.microwintech.boardingtreasure.ui;

import android.app.Activity;
import android.app.Application;
import com.microwintech.boardingtreasure.tool.CommonTool;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Administrator on 2014/11/23.
 */
public class ManageApplication extends Application {
    private static ManageApplication instance;
    private List<Activity> activityList = new LinkedList<Activity>();

    @Override
    public void onCreate() {
        super.onCreate();
        if (instance == null) {
            instance = this;
        }
        File proFile = new File(CommonTool.DATA_FILE_PATH);
        if (!proFile.exists()) {
            proFile.mkdirs();
        }
        File imageFile = new File(CommonTool.DATA_IMAGE_FILE);
        if (!imageFile.exists()) {
            imageFile.mkdirs();
        }
        File videoFile = new File(CommonTool.DATA_VIDEO_FILE);
        if (!videoFile.exists()) {
            videoFile.mkdirs();
        }
        File tempFile = new File(CommonTool.DATA_TEMP_FILE);
        if (!tempFile.exists()) {
            tempFile.mkdirs();
        }
        File cacheFile = new File(CommonTool.DATA_CACHE_FILE);
        if (!cacheFile.exists()) {
            cacheFile.mkdirs();
        }
        File safetyFile = new File(CommonTool.DATA_SAFETY_FILE);
        if (!safetyFile.exists()) {
            safetyFile.mkdirs();
        }
    }

    // 单例模式中获取唯一的MyApplication实例
    public static ManageApplication getInstance() {
        if (null == instance) {
            instance = new ManageApplication();
        }
        return instance;
    }

    // 添加Activity到容器中
    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    // 遍历所有Activity并finish

    public void exit() {
        for (Activity activity : activityList) {
            activity.finish();
        }
        System.exit(0);
    }

    public void finishActivity(Activity activity) {
        activity.finish();
    }

}
