package com.microwintech.boardingtreasure.ui.myjourney;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ListView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.adapter.myjourney.Journey;
import com.microwintech.boardingtreasure.adapter.myjourney.JourneyAdapter;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;
import com.microwintech.boardingtreasure.util.DateUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/11/29.
 * 我的行程
 */
public class MyJourneyActivity extends Activity {
    private ListView journeyLv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_journey);
        journeyLv = (ListView) findViewById(R.id.my_journey_lv);
        ImageButton backIb = (ImageButton) findViewById(R.id.common_top_back_ib);

        UserInfo user = UserCache.getInstance().getUser(MyJourneyActivity.this);
        Map<String, String> mapParam = new HashMap<String, String>();
        mapParam.put("UserGuid", user.getUserGuid());
        VolleyManage.getInstance().methodPost(MyJourneyActivity.this, "正在获取中...", Constant.MY_JOURNEY_TRIP_MESSAGE, mapParam, new JourneyVolleyReal());
        backIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    class JourneyVolleyReal implements VolleyInterface {

        @Override
        public void gainData(VolleyBean bean) {
            List<Journey> journeyList = new ArrayList<Journey>();
            if (bean.isSuccess()) {
                JSONArray parArray = JSON.parseArray(bean.getContent());
                int count = parArray.size();
                for (int i = 0; i < count; i++) {
                    Journey journey = new Journey();
                    JSONObject obj = parArray.getJSONObject(i);
                    journey.setTicketNo(obj.getString("TicketNo"));
                    journey.setName(obj.getString("PassengerName"));
                    journey.setDepDate(obj.getString("DepDate"));
                    journey.setArrDate(obj.getString("ArrDate"));
                    journey.setDepAirportName(obj.getString("DepAirportName"));
                    journey.setArrAirportName(obj.getString("ArrAirportName"));
                    journey.setFlightCode(obj.getString("FlightCode"));
                    journey.setDepAirportCode(obj.getString("DepAirportCode"));
                    journey.setArrAirportCode(obj.getString("ArrAirportCode"));
                    String curDate = DateUtil.getDateNow();
                    boolean result = DateUtil.greaterDate(obj.getString("DepDate"), curDate);
                    journey.setWhetherNeed(result);
                    journeyList.add(journey);
                    //journey.setVoyage();

//                    "TicketNo": "781-2486065432",
//                            "PassengerName": "尚正通",
//                            "DepDate": "2014-12-16",
//                            "ArrDate": null,
//                            "FlightCode": "MU5215",
//                            "DepAirportCode": "HGH",
//                            "DepAirportName": "杭州市",
//                            "ArrAirportCode": "CAN",
//                            "ArrAirportName": "广州市"
                }
            }
            JourneyAdapter journeyAdapter = new JourneyAdapter(MyJourneyActivity.this, journeyList);
            journeyLv.setAdapter(journeyAdapter);
//            JourneyAdapter
        }
    }
}