package com.microwintech.boardingtreasure.ui.mypeony;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;

/**
 * Created by Administrator on 2014/11/2.
 * 意见反馈
 */
public class FeedbackActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback);

        LinearLayout backLl = (LinearLayout) findViewById(R.id.common_top_back_ll);
        backLl.setOnClickListener(new FeedbackOnClick());
    }
    
    class FeedbackOnClick implements OnClickListener{

		@Override
		public void onClick(View v) {
			finish();
		}
    	
    }
}