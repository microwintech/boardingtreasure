package com.microwintech.boardingtreasure.ui.mypeony;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;

/**
 * Created by Administrator on 2014/11/4.
 * 登机牌记录
 */
public class BoardRecordActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.board_record);
        TextView titleTv = (TextView) findViewById(R.id.common_top_title_tv);
        titleTv.setText(R.string.boardrecord_title);


    }
}