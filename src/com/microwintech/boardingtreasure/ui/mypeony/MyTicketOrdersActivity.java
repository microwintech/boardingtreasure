package com.microwintech.boardingtreasure.ui.mypeony;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.adapter.myticketorders.Order;
import com.microwintech.boardingtreasure.adapter.myticketorders.OrderAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014/11/2.
 * 我的机票预订
 */
public class MyTicketOrdersActivity extends Activity {
    private ListView orderLv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_ticket_orders);
        TextView titleTv = (TextView) findViewById(R.id.common_top_title_tv);
        titleTv.setText(R.string.my_ticket_orders_title);
        orderLv = (ListView) findViewById(R.id.my_ticket_orders_lv);
        List<Order> orderList = initData();
        OrderAdapter adapter = new OrderAdapter(this, orderList);
        orderLv.setAdapter(adapter);
    }

    private List<Order> initData() {
        List<Order> orderList = new ArrayList<Order>();
        Order order = new Order();
        order.setStartEndPoint("上海-北京");
        order.setDepartureTime("2014-10-14 19:55");
        order.setAirport("上海虹桥-北京首都");
        order.setFlight("国际航空CA1550");
        order.setTicketPrice("$1290");
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);
        orderList.add(order);

        return orderList;
    }
}