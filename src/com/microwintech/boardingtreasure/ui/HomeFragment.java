package com.microwintech.boardingtreasure.ui;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.ui.boardpass.BoardingPassActivity;
import com.microwintech.boardingtreasure.ui.flightdynamic.FlightDynamicFragment;
import com.microwintech.boardingtreasure.ui.myjourney.MyJourneyActivity;
import com.microwintech.boardingtreasure.ui.planeticket.QueryTicketActivity;

/**
 * Created by Administrator on 2014/11/10.
 * 首页
 */
public class HomeFragment extends BaseFragment {
    public Activity context;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        //办理登机牌
        ImageButton boardingPassBtn = (ImageButton) view.findViewById(R.id.home_boarding_pass_btn);
        boardingPassBtn.setOnClickListener(new JumpClickListener('B'));
        //航班动态
        ImageButton flightDynamicBtn = (ImageButton) view.findViewById(R.id.home_flight_dynamic_btn);
        flightDynamicBtn.setOnClickListener(new JumpClickListener('F'));
        //我的行程
        ImageButton journeyBtn = (ImageButton) view.findViewById(R.id.home_my_journey_btn);
        journeyBtn.setOnClickListener(new JumpClickListener('J'));
        //官网
        ImageButton guangWangBtn = (ImageButton) view.findViewById(R.id.home_guangwang_btn);
        guangWangBtn.setOnClickListener(new JumpClickListener('G'));
    }

    class JumpClickListener implements View.OnClickListener {
        char type;

        JumpClickListener(char type) {
            this.type = type;
        }

        @Override
        public void onClick(View view) {
            switch (type) {
                case 'B': {
                    //办理登机牌
                    Intent intent = new Intent(context, BoardingPassActivity.class);
                    intent.putExtra("flight_code", "");
                    intent.putExtra("flight_date", "");
                    intent.putExtra("flight_data", "none");
                    startActivity(intent);
                    break;
                }
                case 'T': {
                    Intent intent = new Intent(context, QueryTicketActivity.class);
                    startActivity(intent);
                    break;
                }
                case 'F': {
                    FlightDynamicFragment dynamicFragment = new FlightDynamicFragment();
                    switchFragment(dynamicFragment);
                    break;
                }
                case 'J': {
                    //我的行程
                    Intent intent = new Intent(context, MyJourneyActivity.class);
                    startActivity(intent);
//                    //常用乘机人管理
//                    PassengerManageFragment manageFragment = new PassengerManageFragment();
//                    switchFragment(manageFragment);
                    break;
                }
                case 'G': {
//                    PerfectInfoFragment perfectFragment = new PerfectInfoFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("userguid", "8888");
//                    perfectFragment.setArguments(bundle);
//                    switchFragment(perfectFragment);
                    Intent intent = new Intent(context, OfficialWebsiteAcitivy.class);
                    startActivity(intent);

                    break;
                }
            }
        }
    }
}
