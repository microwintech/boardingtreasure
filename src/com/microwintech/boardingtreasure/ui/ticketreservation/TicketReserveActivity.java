package com.microwintech.boardingtreasure.ui.ticketreservation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;

/**
 * Created by Administrator on 2014/11/1.
 * 机票预订
 */
public class TicketReserveActivity extends Activity {
    private TextView chufaTv, daodaTv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ticket_reserve);
        TextView titleTv = (TextView) findViewById(R.id.common_top_title_tv);
        titleTv.setText(R.string.ticket_reserve_title_info);
        chufaTv = (TextView) findViewById(R.id.ticket_reserve_chufa_tv);
        ImageView exchangeIv = (ImageView) findViewById(R.id.ticket_reserve_exchange_iv);
        exchangeIv.setOnClickListener(new RefreshOnClickListener());
        daodaTv = (TextView) findViewById(R.id.ticket_reserve_daoda_tv);
        chufaTv.setOnClickListener(new CityOnClickListener());
        daodaTv.setOnClickListener(new CityOnClickListener());
    }

    class CityOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent mainIntent = new Intent(TicketReserveActivity.this, HotCityActivity.class);
            startActivity(mainIntent);
        }
    }

    class RefreshOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            String chufaStr = chufaTv.getText().toString();
            String daodaStr = daodaTv.getText().toString();
            chufaTv.setText(daodaStr);
            daodaTv.setText(chufaStr);
        }
    }
}