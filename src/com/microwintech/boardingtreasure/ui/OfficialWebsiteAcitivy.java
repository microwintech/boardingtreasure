package com.microwintech.boardingtreasure.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.component.ProgressWebView;

/**
 * Created by Administrator on 2014/12/20.
 */
public class OfficialWebsiteAcitivy extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.official_website);

        ImageButton backIb = (ImageButton) findViewById(R.id.office_top_back_ib);
        backIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ProgressWebView webView = (ProgressWebView) findViewById(R.id.official_wv);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                if (url != null && url.startsWith("http://"))
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        });
        webView.loadUrl("http://www.mudan.biz");
    }
}