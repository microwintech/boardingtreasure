package com.microwintech.boardingtreasure.ui.planeticket;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.platform.common.companent.customcalendar.CalendarInterface;
import com.microwintech.platform.common.companent.customcalendar.CalendarPopupWindow;

/**
 * Created by Administrator on 2014/11/19.
 */
public class QueryTicketActivity extends Activity {
    private Button onewayBtn, returnBtn;
    private TextView timeTv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.query_ticket);
        onewayBtn = (Button) findViewById(R.id.query_ticket_oneway_btn);
        returnBtn = (Button) findViewById(R.id.query_ticket_return_btn);
        onewayBtn.setOnClickListener(new ChangeExchangeClickListener('O'));
        onewayBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_pre));
        returnBtn.setOnClickListener(new ChangeExchangeClickListener('R'));
        timeTv = (TextView) findViewById(R.id.query_ticket_reserve_time_tv);
        timeTv.setOnClickListener(new SelectTimeClickListener());
    }

    class SelectTimeClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            new CalendarPopupWindow(QueryTicketActivity.this, timeTv, new GainCalendar());
        }
    }

    class GainCalendar implements CalendarInterface {

        @Override
        public void gainSelectedDate(String selectDate, String weekDay) {
            System.out.println("选中:" + selectDate + "...." + weekDay);
        }
    }


    class ChangeExchangeClickListener implements View.OnClickListener {
        char type;

        ChangeExchangeClickListener(char type) {
            this.type = type;
        }

        @Override
        public void onClick(View view) {
            switch (type) {
                case 'O': {
                    onewayBtn.setTextColor(getResources().getColor(R.color.gainsboro));
                    returnBtn.setTextColor(getResources().getColor(R.color.white));
                    returnBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_nor));
                    onewayBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_pre));

                    break;
                }
                case 'R': {
                    onewayBtn.setTextColor(getResources().getColor(R.color.white));
                    returnBtn.setTextColor(getResources().getColor(R.color.gainsboro));
                    onewayBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_nor));
                    returnBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_pre));

                    break;
                }
            }
        }
    }
}