package com.microwintech.boardingtreasure.ui;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;
import com.microwintech.boardingtreasure.ui.boardpass.MyBoardingPassActivity;
import com.microwintech.boardingtreasure.ui.login.LoginActivity;
import com.microwintech.boardingtreasure.ui.myjourney.MyJourneyActivity;
import com.microwintech.boardingtreasure.ui.mypeony.FeedbackActivity;
import com.microwintech.boardingtreasure.ui.passengermanage.PassengerManageActivity;
import com.microwintech.boardingtreasure.ui.perfectinfomation.PerfectInfoActivity;
import com.microwintech.boardingtreasure.ui.register.RegisterActivity;
import com.microwintech.boardingtreasure.util.StringUtil;
import com.microwintech.boardingtreasure.util.ToastUtil;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;
import com.microwintech.platform.pay.PayDemoActivity;

/**
 * Created by Administrator on 2014/11/10.
 */
public class MyPeonyFragment extends Fragment {
	private Context context;
	private RelativeLayout eventRl;
	private TextView titleTv;
	private TextView nameTv;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.context = getActivity();
		View view = inflater.inflate(R.layout.fragment_my_peony, container, false);
		// 注册广播
		RefreshLoginReceiver loginReceiver = new RefreshLoginReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constant.LOGIN_REFRESH_ACTION_RECEIVER); // 只有持有相同的action的接受者才能接收此广播
		context.registerReceiver(loginReceiver, filter);

		initView(view);
		return view;
	}

	private void initView(View view) {
		titleTv = (TextView) view.findViewById(R.id.my_aolian_title_tv);
		nameTv = (TextView) view.findViewById(R.id.my_penoy_name_tv);
		eventRl = (RelativeLayout) view.findViewById(R.id.my_penoy_event_rl);
		UserInfo userInfo = UserCache.getInstance().getUser(context);
		if (userInfo.isWhetherLogin()) {
			titleTv.setText(R.string.my_penoy_welcome_back);
			eventRl.setVisibility(View.GONE);
			nameTv.setVisibility(View.VISIBLE);
			nameTv.setText(userInfo.getName());
		} else {
			titleTv.setText(R.string.my_penoy_welcome_penoy);
			eventRl.setVisibility(View.VISIBLE);
			nameTv.setVisibility(View.GONE);
		}
		// 登录
		TextView loginTv = (TextView) view.findViewById(R.id.my_aolian_login_tv);
		loginTv.setOnClickListener(new LoginRegisterClickListener('L'));
		// 注册
		TextView registerTv = (TextView) view.findViewById(R.id.my_aolian_register_tv);
		registerTv.setOnClickListener(new LoginRegisterClickListener('R'));
		// 我的登机牌
		EffectsButton boardingPassEb = (EffectsButton) view.findViewById(R.id.my_boarding_pass_eb);
		boardingPassEb.setOnClickListener(new PeonyOnClickListener('B'));
		// 常用乘机人管理
		EffectsButton peopleManagementEb = (EffectsButton) view.findViewById(R.id.my_penoy_people_management_eb);
		peopleManagementEb.setOnClickListener(new PeonyOnClickListener('M'));
		// 我的资料
		EffectsButton informationEb = (EffectsButton) view.findViewById(R.id.my_penoy_myinformation_eb);
		informationEb.setOnClickListener(new PeonyOnClickListener('I'));
		// 意见反馈
		EffectsButton feedbackEb = (EffectsButton) view.findViewById(R.id.my_penoy_feedback_eb);
		feedbackEb.setOnClickListener(new PeonyOnClickListener('F'));

		// //关于牡丹
		// EffectsButton peonyEb = (EffectsButton)
		// view.findViewById(R.id.my_penoy_about_peony_eb);
		// peonyEb.setOnClickListener(new PeonyOnClickListener('P'));
		// //我的行程
		// EffectsButton journeyEb = (EffectsButton)
		// view.findViewById(R.id.my_aolian_my_my_journey_eb);
		// journeyEb.setOnClickListener(new PeonyOnClickListener('J'));

	}

	private void switchFragment(Fragment fragment) {
		if (getActivity() == null) {
			return;
		}
		if (getActivity() instanceof MainActivity) {
			MainActivity ma = (MainActivity) getActivity();
			ma.switchContent(fragment);
		}
	}

	class LoginRegisterClickListener implements View.OnClickListener {
		char type;

		LoginRegisterClickListener(char type) {
			this.type = type;
		}

		@Override
		public void onClick(View view) {
			switch (type) {
			case 'L': {
				// 登录
				Intent intent = new Intent(context, LoginActivity.class);
				context.startActivity(intent);

				onDestroy();
				// LoginActivity
				break;
			}
			case 'R': {
				// 注册
				Intent intent = new Intent(context, RegisterActivity.class);
				context.startActivity(intent);
				onDestroy();
				break;
			}
			}
		}
	}

	class PeonyOnClickListener implements View.OnClickListener {
		char typeStart;

		PeonyOnClickListener(char typeStart) {
			this.typeStart = typeStart;
		}

		@Override
		public void onClick(View view) {
			UserInfo user = UserCache.getInstance().getUser(context);
			if (StringUtil.isBlank(user.getUserGuid())) {
				ToastUtil.TextToast(context, "请您先登录本系统", Toast.LENGTH_SHORT);
				return;
			}

			switch (typeStart) {
			case 'B': {
				// 我的登机牌
				Intent intent = new Intent(context, MyBoardingPassActivity.class);
				intent.putExtra("UserGuid", user.getUserGuid());
				startActivity(intent);
				break;
			}
			case 'M': {
				// 常用乘机人管理
				Intent mainIntent = new Intent(context, PassengerManageActivity.class);
				startActivity(mainIntent);
				break;
			}
			case 'F': {
				// 意见反馈
				Intent mainIntent = new Intent(context, FeedbackActivity.class);
				startActivity(mainIntent);
				break;
			}
			// case 'J': {
			// Intent intent = new Intent(context, MyJourneyActivity.class);
			// startActivity(intent);
			// break;
			// }
			case 'I': {
				UserCache.getInstance().getUser(context);
				Intent intent = new Intent(context, PerfectInfoActivity.class);
				intent.putExtra("userguid", user.getUserGuid());
				intent.putExtra("phoneNumber", user.getPhoneNumber());
				intent.putExtra("type", "my");
				startActivity(intent);
				break;
			}
			// case 'P': {
			// Intent intent = new Intent(context, PayDemoActivity.class);
			//
			// startActivity(intent);
			// break;
			// }
			}

		}
	}

	@Override
	public void onResume() {
		super.onResume();
		System.out.println("LeftFragment onResume");
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("LeftFragment onPause");
	}

	@Override
	public void onStop() {
		super.onStop();
		System.out.println("LeftFragment onStop");
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	public class RefreshLoginReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			UserInfo userInfo = UserCache.getInstance().getUser(context);
			if (userInfo.isWhetherLogin()) {
				titleTv.setText(R.string.my_penoy_welcome_back);
				eventRl.setVisibility(View.GONE);
				nameTv.setVisibility(View.VISIBLE);
				nameTv.setText(userInfo.getName());
			} else {
				titleTv.setText(R.string.my_penoy_welcome_penoy);
				eventRl.setVisibility(View.VISIBLE);
				nameTv.setVisibility(View.GONE);
			}
		}
	}
}
