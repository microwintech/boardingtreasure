package com.microwintech.boardingtreasure.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.db.bean.UserInfo;
import com.microwintech.boardingtreasure.db.cache.UserCache;
import com.microwintech.boardingtreasure.ui.register.RegisterActivity;
import com.microwintech.boardingtreasure.util.JsonUtil;
import com.microwintech.boardingtreasure.util.StringUtil;
import com.microwintech.boardingtreasure.util.ToastUtil;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2014/12/7.
 */
public class LoginActivity extends Activity {
	private EditText phoneNumberEt, phonePsdEt;
	private EditText dynamicNumberEt, dynamicPsdEt;
	private String verificationCode = "";
	private LinearLayout phoneFl, dynamicFl;
	private Button phoneBtn, dynamicBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		TextView registerTv = (TextView) findViewById(R.id.login_register_tv);
		registerTv.setOnClickListener(new RegisterListener());
		phoneBtn = (Button) findViewById(R.id.login_phone_btn);
		phoneBtn.setTextColor(this.getResources().getColor(R.color.white));
		dynamicBtn = (Button) findViewById(R.id.login_dynamic_btn);
		dynamicBtn.setTextColor(this.getResources().getColor(R.color.black));
		phoneBtn.setOnClickListener(new ChangeLoginTypeListener('P'));
		dynamicBtn.setOnClickListener(new ChangeLoginTypeListener('D'));
		phoneFl = (LinearLayout) findViewById(R.id.login_phone_fl);
		dynamicFl = (LinearLayout) findViewById(R.id.login_dynamic_fl);

		// 手机登陆
		phoneNumberEt = (EditText) findViewById(R.id.login_phone_number_et);
		phonePsdEt = (EditText) findViewById(R.id.login_phone_password_et);
		// 动态登陆
		dynamicNumberEt = (EditText) findViewById(R.id.login_dynamic_number_et);
		dynamicPsdEt = (EditText) findViewById(R.id.login_dynamic_password_et);
		EffectsButton verifyBtn = (EffectsButton) findViewById(R.id.login_verify_btn);
		verifyBtn.setOnClickListener(new VerifyOnClickListener());
		//
		EffectsButton loginBtn = (EffectsButton) findViewById(R.id.login_submit_btn);
		loginBtn.setOnClickListener(new LoginListener());
	}

	class VerifyOnClickListener implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			String number = dynamicNumberEt.getText().toString();
			if (StringUtil.isNotBlank(number)) {
				Map<String, String> mapParam = new HashMap<String, String>();
				mapParam.put("MobilePhone", number);
				VolleyManage.getInstance().methodPost(LoginActivity.this, "正在获取中...", Constant.VERIFY_GET_VERIFICATION, mapParam, new VerifyListener());
			} else {
				ToastUtil.TextToast(LoginActivity.this, "请您输入手机号", Toast.LENGTH_SHORT);
			}
		}
	}

	class VerifyListener implements VolleyInterface {

		@Override
		public void gainData(VolleyBean bean) {
			if (bean.isSuccess()) {
				verificationCode = JsonUtil.getValue(bean.getContent(), "verificationCode");
			} else {
				ToastUtil.TextToast(LoginActivity.this, "系统忙，请您稍后再试！", Toast.LENGTH_SHORT);
			}
		}
	}

	class RegisterListener implements View.OnClickListener {

		@Override
		public void onClick(View view) {
			// 注册
			Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
			startActivity(intent);
			finish();
		}
	}

	class LoginListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			if (View.VISIBLE == phoneFl.getVisibility()) {
				//
				String phoneNumber = phoneNumberEt.getText().toString();
				String phonePsd = phonePsdEt.getText().toString();
				if (StringUtil.isBlank(phoneNumber)) {
					ToastUtil.TextToast(LoginActivity.this, "请您输入手机号码", Toast.LENGTH_SHORT);
					return;
				}
				if (StringUtil.isBlank(phonePsd)) {
					ToastUtil.TextToast(LoginActivity.this, "请您输入密码", Toast.LENGTH_SHORT);
					return;
				}
				Map<String, String> mapParam = new HashMap<String, String>();
				mapParam.put("MobilePhone", phoneNumber);
				mapParam.put("PassWord", phonePsd);
				mapParam.put("Type", "1");
				VolleyManage.getInstance().methodPost(LoginActivity.this, "正在获取中...", Constant.USER_LOGIN_USER, mapParam, new LoginResulListener());

			}
			if (View.VISIBLE == dynamicFl.getVisibility()) {
				String dynamicNumber = dynamicNumberEt.getText().toString();
				String dynamicPsd = dynamicPsdEt.getText().toString();
				if (StringUtil.isBlank(dynamicNumber)) {
					ToastUtil.TextToast(LoginActivity.this, "请您输入手机号码", Toast.LENGTH_SHORT);

					return;
				}
				if (StringUtil.isBlank(dynamicPsd)) {
					ToastUtil.TextToast(LoginActivity.this, "请您输入验证码", Toast.LENGTH_SHORT);
					return;
				}
				if (!verificationCode.equals(dynamicPsd)) {
					ToastUtil.TextToast(LoginActivity.this, "验证码不正确", Toast.LENGTH_SHORT);
					return;
				}

				Map<String, String> mapParam = new HashMap<String, String>();
				mapParam.put("MobilePhone", dynamicNumber);
				mapParam.put("PassWord", dynamicPsd);
				mapParam.put("Type", "2");
				VolleyManage.getInstance().methodPost(LoginActivity.this, "正在获取中...", Constant.USER_LOGIN_USER, mapParam, new LoginResulListener());
			}
		}
	}

	class LoginResulListener implements VolleyInterface {

		@Override
		public void gainData(VolleyBean bean) {
			if (bean.isSuccess()) {
				// [{"CardNo":"3406261198604098731","CardType":"1","CreateDate":"/Date(1418044487000)/","IsDelete":false,"Mobile":"15601589361","Name":"任伟伟\n","OftenContactID":"4ed4efa0-75bc-4fce-91f4-e8771ee431d4","PreferArea":"前排","PreferSeat":"靠窗","Type":1,"UserGuid":"c1fd020a-459f-43ad-bcf9-4b5b7ccaad7d"}]
				UserInfo user = new UserInfo();
				JSONArray parArray = JSON.parseArray(bean.getContent());
				JSONObject childObj = parArray.getJSONObject(0);
				user.setCardNo(childObj.getString("CardNo"));
				user.setOftenContactID(childObj.getString("OftenContactID"));
				user.setPhoneNumber(childObj.getString("Mobile"));
				user.setName(childObj.getString("Name"));
				// "PreferArea":"前排","PreferSeat":"靠窗"
				user.setUserGuid(childObj.getString("UserGuid"));
				user.setPreferSeat(childObj.getString("PreferSeat"));
				user.setPreferArea(childObj.getString("PreferArea"));
				user.setWhetherLogin(true);
				UserCache.getInstance().addUser(LoginActivity.this, user);
				Intent intent = new Intent(); // Itent就是我们要发送的内容
				intent.setAction(Constant.LOGIN_REFRESH_ACTION_RECEIVER); // 设置你这个广播的action，只有和这个action一样的接受者才能接受者才能接收广播
				sendBroadcast(intent); // 发送广播
				finish();
			} else {
				ToastUtil.TextToast(LoginActivity.this, bean.getMessage(), Toast.LENGTH_SHORT);
			}
		}
	}

	class ChangeLoginTypeListener implements View.OnClickListener {
		char type;

		ChangeLoginTypeListener(char type) {
			this.type = type;
		}

		@Override
		public void onClick(View view) {
			switch (type) {
			case 'P': {
				phoneFl.setVisibility(View.VISIBLE);
				dynamicFl.setVisibility(View.GONE);

				phoneBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_pre));
				dynamicBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_nor));
				phoneBtn.setTextColor(getResources().getColor(R.color.white));
				dynamicBtn.setTextColor(getResources().getColor(R.color.black));
				break;
			}
			case 'D': {
				phoneFl.setVisibility(View.GONE);
				dynamicFl.setVisibility(View.VISIBLE);
				dynamicBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_pre));
				phoneBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_nor));
				phoneBtn.setTextColor(getResources().getColor(R.color.black));
				dynamicBtn.setTextColor(getResources().getColor(R.color.white));
				break;
			}
			}
		}
	}
}