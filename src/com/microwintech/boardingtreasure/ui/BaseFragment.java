package com.microwintech.boardingtreasure.ui;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Administrator on 2014/11/15.
 */
public class BaseFragment extends Fragment {

    public void switchFragment(Fragment fragment) {
        if (getActivity() == null) {
            return;
        }
        if (getActivity() instanceof MainActivity) {
            MainActivity ma = (MainActivity) getActivity();
            ma.switchContent(fragment);
        }
    }
}
