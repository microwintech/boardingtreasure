package com.microwintech.boardingtreasure.ui.flightdynamic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.adapter.flightdynamic.FlightInfo;
import com.microwintech.boardingtreasure.adapter.flightdynamic.FlightInfoAdapter;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.common.client.VolleyManage;
import com.microwintech.boardingtreasure.common.component.HVListView;
import android.view.View;
import android.view.ViewGroup;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.common.security.SecurityUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/11/6.
 * 航班动态查询列表
 */
public class FlightQueryActivity extends Activity {
    private TextView numberTv;
    private ListView queryHvLv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flight_query);
        Intent intent = getIntent();
        TextView flightInfoTv = (TextView) findViewById(R.id.flight_query_info_tv);
        String searchType = intent.getStringExtra("searchType");
        numberTv = (TextView) findViewById(R.id.flight_query_number_tv);
        queryHvLv = (ListView) findViewById(R.id.flight_query_hv_lv);
        ImageButton backIb = (ImageButton) findViewById(R.id.common_top_back_ib);
        backIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Map<String, String> mapParam = new HashMap<String, String>();
        if ("number".equals(searchType)) {
            String flightNumber = intent.getStringExtra("flightNumber");
            mapParam.put("flightCode", flightNumber);
            flightInfoTv.setText(flightNumber);
            VolleyManage.getInstance().methodPost(this, "正在加载中...", Constant.FLIGHT_GET_FLIGHT_CODE, mapParam, new FlightVolleyListener());
        }
        if ("city".equals(searchType)) {
            String fromCity = intent.getStringExtra("fromCity");
            String toCity = intent.getStringExtra("toCity");
            flightInfoTv.setText(fromCity + "-" + toCity);
            mapParam.put("fromCityName", fromCity);
            mapParam.put("toCityName", toCity);
            VolleyManage.getInstance().methodPost(this, "正在加载中...", Constant.FLIGHT_GET_FLIGHT_CITY, mapParam, new FlightVolleyListener());
        }

    }

    class FlightVolleyListener implements VolleyInterface {
        @Override
        public void gainData(VolleyBean bean) {
            if (bean.isSuccess()) {
                if (bean.getContent().length() < 6) {
                    return;
                }
                List<FlightInfo> flightList = new ArrayList<FlightInfo>();
                JSONArray parArray = JSON.parseArray(bean.getContent());
                int count = parArray.size();
                numberTv.setText(String.valueOf(count));
                for (int i = 0; i < count; i++) {
                    JSONObject jsonObj = parArray.getJSONObject(i);
                    flightList.add(jsonToObj(jsonObj));
                }
                FlightInfoAdapter infoAdapter = new FlightInfoAdapter(FlightQueryActivity.this, flightList);
                queryHvLv.setAdapter(infoAdapter);
            }

            //{"ActArrTime":"09:15","ActDepTime":"07:34","ArrCity":"北京","ArrTerminal":"","CarrierName":"中国邮政航空公司","DepCity":"上海浦东","DepTerminal":"",
            // "FlightCode":"8Y9022","FlightState":"到达","PlanArrTime":"09:24","PlanDepTime":"04:25"}
        }
    }

    public FlightInfo jsonToObj(JSONObject json) {
        FlightInfo info = new FlightInfo();
        info.setActArrTime(json.getString("ActArrTime"));
        info.setActDepTime(json.getString("ActDepTime"));
        info.setArrCity(json.getString("ArrCity"));
        info.setArrTerminal(json.getString("ArrTerminal"));
        info.setCarrierName(json.getString("CarrierName"));
        info.setDepCity(json.getString("DepCity"));
        info.setDepTerminal(json.getString("DepTerminal"));
        info.setFlightCode(json.getString("FlightCode"));
        info.setFlightState(json.getString("FlightState"));
        info.setPlanArrTime(json.getString("PlanArrTime"));
        info.setPlanDepTime(json.getString("PlanDepTime"));
        return info;
    }
}