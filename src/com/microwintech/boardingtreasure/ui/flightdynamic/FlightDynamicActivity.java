package com.microwintech.boardingtreasure.ui.flightdynamic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.ui.MainActivity;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

/**
 * Created by Administrator on 2014/11/4.
 * 航班动态
 */
public class FlightDynamicActivity extends Activity {
    private Button takeOffBtn, numberBtn;
    private FrameLayout takeOffFl, numberFl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flight_dynamic);
        TextView titleTv = (TextView) findViewById(R.id.common_top_title_tv);
        titleTv.setText(R.string.flight_dynamic_title);

        takeOffBtn = (Button) findViewById(R.id.flight_takeoff_btn);
        takeOffBtn.setBackgroundColor(getResources().getColor(R.color.lightgrey));
        takeOffBtn.setOnClickListener(new SearchOnClickListener('T'));
        numberBtn = (Button) findViewById(R.id.flight_number_btn);
        numberBtn.setBackgroundColor(getResources().getColor(R.color.green));
        numberBtn.setOnClickListener(new SearchOnClickListener('N'));

        takeOffFl = (FrameLayout) findViewById(R.id.flight_takeoff_fl);
        numberFl = (FrameLayout) findViewById(R.id.flight_number_fl);
        numberFl.setVisibility(View.GONE);
        EffectsButton searchEb = (EffectsButton) findViewById(R.id.flight_search_btn);
        searchEb.setOnClickListener(new SearchOnClickListener('S'));

    }

    class SearchOnClickListener implements View.OnClickListener {
        char type;

        SearchOnClickListener(char type) {
            this.type = type;
        }

        @Override
        public void onClick(View view) {
            switch (type) {
                case 'N': {
                    takeOffBtn.setBackgroundColor(getResources().getColor(R.color.lightgrey));
                    numberBtn.setBackgroundColor(getResources().getColor(R.color.green));
                    takeOffFl.setVisibility(View.VISIBLE);
                    numberFl.setVisibility(View.GONE);
                    break;
                }
                case 'T': {
                    takeOffBtn.setBackgroundColor(getResources().getColor(R.color.green));
                    numberBtn.setBackgroundColor(getResources().getColor(R.color.lightgrey));
                    takeOffFl.setVisibility(View.GONE);
                    numberFl.setVisibility(View.VISIBLE);
                    break;
                }
                case 'S': {
                    Intent mainIntent = new Intent(FlightDynamicActivity.this, FlightQueryActivity.class);
                    startActivity(mainIntent);
                    break;
                }
            }
        }
    }

}