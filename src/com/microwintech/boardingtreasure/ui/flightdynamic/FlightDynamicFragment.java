package com.microwintech.boardingtreasure.ui.flightdynamic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.lang.Constant;
import com.microwintech.boardingtreasure.ui.BaseFragment;
import com.microwintech.boardingtreasure.ui.ticketreservation.HotCityActivity;
import com.microwintech.boardingtreasure.util.StringUtil;
import com.microwintech.platform.common.companent.effectsbutton.EffectsButton;

/**
 * Created by Administrator on 2014/11/15.
 * 航班动态
 */
public class FlightDynamicFragment extends BaseFragment {
    public Activity context;
    private Button takeOffBtn, numberBtn;
    private FrameLayout takeOffFl, numberFl;
    private TextView setoutCityTv, arriveCityTv;
    private EditText numberEt;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();
        View view = inflater.inflate(R.layout.flight_dynamic, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        TextView titleTv = (TextView) view.findViewById(R.id.common_top_title_tv);
        titleTv.setText(R.string.flight_dynamic_title);
        takeOffBtn = (Button) view.findViewById(R.id.flight_takeoff_btn);
        takeOffBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_pre));
        takeOffBtn.setOnClickListener(new SearchOnClickListener('T'));
        takeOffBtn.setTextColor(this.getResources().getColor(R.color.white));
        numberBtn = (Button) view.findViewById(R.id.flight_number_btn);
        numberBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_nor));
        numberBtn.setOnClickListener(new SearchOnClickListener('N'));
        numberBtn.setTextColor(this.getResources().getColor(R.color.black));
        takeOffFl = (FrameLayout) view.findViewById(R.id.flight_takeoff_fl);
        numberFl = (FrameLayout) view.findViewById(R.id.flight_number_fl);
        numberFl.setVisibility(View.GONE);
        //出发城市
        setoutCityTv = (TextView) view.findViewById(R.id.flight_setout_city_tv);
        setoutCityTv.setOnClickListener(new CityClickListener('S'));
        //到达城市
        arriveCityTv = (TextView) view.findViewById(R.id.flight_arrive_city_tv);
        arriveCityTv.setOnClickListener(new CityClickListener('A'));
        numberEt = (EditText) view.findViewById(R.id.flight_dynamic_number_et);
        EffectsButton searchEb = (EffectsButton) view.findViewById(R.id.flight_search_btn);
        searchEb.setOnClickListener(new SearchOnClickListener('S'));
    }

    class CityClickListener implements View.OnClickListener {
        char type;

        CityClickListener(char type) {
            this.type = type;
        }

        @Override
        public void onClick(View view) {
            switch (type) {
                case 'S': {
                    Intent hotIntent = new Intent(context, HotCityActivity.class);
                    hotIntent.putExtra("type", Constant.ACT_RES_FLIGHT_DYNAMIC_SETOUT_CITY_CODE);
                    startActivityForResult(hotIntent, Constant.ACT_RES_FLIGHT_DYNAMIC_SETOUT_CITY_CODE);
                    break;
                }
                case 'A': {
                    Intent hotIntent = new Intent(context, HotCityActivity.class);
                    hotIntent.putExtra("type", Constant.ACT_RES_FLIGHT_DYNAMIC_ARRIVE_CITY_CODE);
                    startActivityForResult(hotIntent, Constant.ACT_RES_FLIGHT_DYNAMIC_ARRIVE_CITY_CODE);
                    break;
                }
            }
        }
    }

    class SearchOnClickListener implements View.OnClickListener {
        char type;

        SearchOnClickListener(char type) {
            this.type = type;
        }

        @Override
        public void onClick(View view) {
            switch (type) {
                case 'N': {
                    takeOffBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_nor));
                    numberBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_pre));
                    takeOffFl.setVisibility(View.GONE);
                    numberFl.setVisibility(View.VISIBLE);
                    takeOffBtn.setTextColor(getResources().getColor(R.color.black));
                    numberBtn.setTextColor(getResources().getColor(R.color.white));
                    break;
                }
                case 'T': {
                    takeOffBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_pre));
                    numberBtn.setBackgroundColor(getResources().getColor(R.color.common_tab_btn_color_nor));
                    takeOffFl.setVisibility(View.VISIBLE);
                    numberFl.setVisibility(View.GONE);
                    numberBtn.setTextColor(getResources().getColor(R.color.black));
                    takeOffBtn.setTextColor(getResources().getColor(R.color.white));
                    break;
                }
                case 'S': {
                    if (View.VISIBLE == numberFl.getVisibility()) {
                        //航班号查询
                        //flight_dynamic_number_et
                        if (StringUtil.isNotBlank(numberEt.getText().toString())) {
                            Intent intent = new Intent(context, FlightQueryActivity.class);
                            intent.putExtra("flightNumber", numberEt.getText().toString());
                            intent.putExtra("searchType", "number");
                            startActivity(intent);
                        }
                    } else {
                        Intent intent = new Intent(context, FlightQueryActivity.class);
                        intent.putExtra("fromCity", setoutCityTv.getText().toString());
                        intent.putExtra("toCity", arriveCityTv.getText().toString());
                        intent.putExtra("searchType", "city");
                        startActivity(intent);
                    }
                    break;
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            String cityName = data.getStringExtra("cityName");
            switch (requestCode) {
                case Constant.ACT_RES_FLIGHT_DYNAMIC_SETOUT_CITY_CODE: {
                    setoutCityTv.setText(cityName);
//                    Toast.makeText(context, cityName + 7001, Toast.LENGTH_LONG).show();
                    break;
                }
                case Constant.ACT_RES_FLIGHT_DYNAMIC_ARRIVE_CITY_CODE: {
                    arriveCityTv.setText(cityName);
//                    Toast.makeText(context, cityName + 7002, Toast.LENGTH_LONG).show();
                    break;
                }
            }
        }
    }
}
