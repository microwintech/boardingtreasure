package com.microwintech.boardingtreasure.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import com.microwintech.boardingtreasure.R;
import com.microwintech.boardingtreasure.common.client.VolleyBean;
import com.microwintech.boardingtreasure.common.client.VolleyInterface;
import com.microwintech.boardingtreasure.tool.CommonTool;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by Administrator on 2014/10/22.
 */
public class WelcomeActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.welcome);
//        new CheckAsyncTask().execute();
        new Handler().postDelayed(timeRunnable, 2000);
//        String source = "{\"name\":\"wangzhenzhu\",\"telephone\":\"13818342667\"}";
//        final byte[] param = SecurityUtil.encryptByPublicKey(this, source);
//        new Thread(new Runnable() {
//            public void run() {
//                System.out.println("cccccccc" + ConnectionManage.methodPost(Constant.USER_GET_USER_BY_JSON, bt));
//
//            }
//        }).start();
//        VolleyManage.getInstance().methodPost(this, "正在注册中...", Constant.USER_GET_USER_BY_JSON, param, new RegisterVerificationVolleyListener());
    }

    class CheckAsyncTask extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {
            createDatabase();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Intent mainIntent = new Intent(WelcomeActivity.this, MainActivity.class);
            startActivity(mainIntent);
            finish();
        }
    }

    class RegisterVerificationVolleyListener implements VolleyInterface {
        @Override
        public void gainData(VolleyBean bean) {
            System.out.println(bean.getContent());
        }
    }

    Runnable timeRunnable = new Runnable() {
        @Override
        public void run() {
            createDatabase();
            Intent mainIntent = new Intent(WelcomeActivity.this, MainActivity.class);
            startActivity(mainIntent);
            finish();
        }
    };

    //创建数据库
    private void createDatabase() {
        try {
            String path = CommonTool.DATA_SAFETY_FILE;
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdir();
            }
            File file = new File(path, "city.db");
            if (!file.exists()) {
                InputStream inputStream = getResources().getAssets().open("city.zip");
                FileOutputStream fos = new FileOutputStream(file);
                ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(inputStream));
                BufferedInputStream b = new BufferedInputStream(zipInputStream);
                //特别注意
                ZipEntry zipEntry = zipInputStream.getNextEntry();
                int count;
                byte[] buffer = new byte[1024];
                while ((count = b.read(buffer)) > 0) {
                    fos.write(buffer, 0, count);
                }
                inputStream.close();
                fos.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}